#include "Assert.hpp"

#include "Variant.hpp"
#include "type_traits/IsSame.hpp"

#include <string>
#include <iostream>
#include <vector>

#include <memory>


using namespace stle;


void test()
{
    using VariantType = Variant<int, double, std::string>;
    VariantType variant{};
    assert(variant.is<int>());

    variant = 12;
    assert(variant.as<int>() == 12);

    variant = VariantType(2.0);
    assert(variant.as<double>() == 2.0);
    assert(variant.is<double>());

    variant = std::string{"This is a string!"};
    assert(variant.as<std::string>() == std::string{"This is a string!"});
    assert(variant.is<std::string>());

    auto variant_copy = variant;
    assert(isSameType(variant, variant_copy));
    assert(variant_copy.as<std::string>() == std::string{"This is a string!"});

    std::string movedString = "Moved string";
    variant_copy = std::move(movedString);

    auto returnConstructor = [](const double str){
        return VariantType(str);
    };

    auto f = [](const auto val) {
        using T = decltype(val);

        if (isSame<T, const std::string>)
            return 2;
        else if (isSame<T, const int>)
            return 0;
        else if (isSame<T, const double>)
            return 1;

        return -1;
    };

    int apply_res = variant.apply<int>(f);
    assert(apply_res == 2);

    //auto m = match<VariantType::Types::Has>([](){return true;});
    //auto m2 = match<double>([](){return true;});*//*

    int match_res = variant.match<int>(
            match::is<int>         | [](const int i){ return 0; },
            match::is<double>      | [](const double i){ return 1; },
            match::is<std::string> | [](const std::string &s){ return 2; }
    );

    assert(match_res == 2);

    variant = 1;
    int match_res2 = variant.match<int>(
            match::match<std::is_integral> | [](const auto v){ return 111; },
            match::any                     | [](const auto v){ return 1; }
    );

    assert(match_res2 == 111);

    int match_res3 = variant.match<int>(
            match::match<std::is_integral> |= 111,
            match::any                     |= 1
    );

    assert(match_res3 == 111);

    {
        VariantType defaultVariant{};
        assert(defaultVariant.as<int>() == 0);
        defaultVariant = 100;

        auto anotherVariant = std::move(defaultVariant);
        assert(anotherVariant.as<int>() == 100);
        assert(defaultVariant.as<int>() == 0);
    }
}

template <class T> using RecursiveContainer = std::vector<T>;

void recursiveVariantTest()
{
    //assert(VariantGetType<int>::value == false);
    //assert(VariantGetType<RecursiveVariantStdContainer<std::vector>>::value);

    using RecursiveType = RecursiveVariant<RecursiveContainer>;
    using VariantType = Variant<int, double, RecursiveType>;

    std::vector<VariantType> vec{
            VariantType(1),
            VariantType(2.0)
    };

    assert(vec[0].as<int>() == 1);
    assert(vec[1].as<double>() == 2.0);

    std::vector<VariantType> vec_copy{vec};

    assert(vec_copy[0].as<int>() == 1);
    assert(vec_copy[1].as<double>() == 2.0);
    assert(vec[0].as<int>() == 1);
    assert(vec[1].as<double>() == 2.0);

    VariantType v{};
    v = vec;
    assert(!vec.empty());
    assert(vec[0].as<int>() == 1);
    assert(vec[1].as<double>() == 2.0);

    assert(v.is<std::vector<VariantType>>());

    int res = v.match<int>(
            match::is<std::vector<VariantType>> | [](const std::vector<VariantType> &vec) {
                assert(vec[0].as<int>() == 1);
                assert(vec[1].as<double>() == 2.0);
                return 1;
            },
            match::any | [](const auto &vec) {
                return 0;
            }
    );

    assert(res == 1);

    std::vector<VariantType> &current = v.as<std::vector<VariantType>>();

    assert(current.size() == 2);
    assert(current[0].as<int>() == 1);
    assert(current[1].as<double>() == 2.0);


    VariantType v2{std::move(vec_copy)};
    //v2.set();
    assert(vec_copy.empty());
    assert(v2.is<std::vector<VariantType>>());
    std::vector<VariantType> &current2  = v2.as<std::vector<VariantType>>();
    assert(current2.size() == 2);
    assert(current2[0].as<int>() == 1);
    assert(current2[1].as<double>() == 2.0);

    v = 10.0;
    assert(v.is<double>());
    assert(v.as<double>() == 10.0);

    v = 11.0;
    assert(v.is<double>());
    assert(v.as<double>() == 11.0);

    v = vec;
    assert(v.is<std::vector<VariantType>>());
}


int main(int argc, const char ** args)
{
    test();
    recursiveVariantTest();
    return 0;
}
