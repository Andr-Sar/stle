#include "Assert.hpp"

#include "TypeList.hpp"
#include "type_traits/IsSame.hpp"


using namespace stle;


struct TestStruct
{
};


void test()
{
    {
        using List = TypeList<int, bool, char>;

        assert((isSame<typename List::At<0>::Type, int>));
        assert((isSame<typename List::At<1>::Type, bool>));
        assert((isSame<typename List::At<2>::Type, char>));
        assert((isSame<typename List::At<3>::Type, None>));
        assert((isSame<typename List::At<100>::Type, None>));

        assert(List::Has<int>::value);
        assert(List::Has<bool>::value);
        assert(List::Has<char>::value);
        assert(!List::Has<double>::value);

        assert((isSame<typename List::Append<double>::Type, TypeList<int, bool, char, double>>));

        using OneElementTypeList = TypeList<float>;
        assert(OneElementTypeList::Has<float>::value);
        assert((isSame<typename OneElementTypeList::At<0>::Type, float>));
    }
}


int main(int argc, const char ** args)
{
    test();
    return 0;
}
