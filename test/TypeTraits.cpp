#include "Assert.hpp"

#include "type_traits/all.hpp"


using namespace stle;


struct TestStruct
{
};


void isPointerTest()
{
    assert(isPointer<int*>);
    assert(isPointer<void*>);
    assert(!isPointer<int>);
    assert(!isPointer<TestStruct>);
    assert(isPointer<TestStruct*>);
    assert(isPointer<const TestStruct*>);
    assert(isPointer<const TestStruct* const>);
}


void isReferenceTest()
{
    assert(isLvalueReference<int&>);
    assert(!isLvalueReference<int>);
    assert(!isLvalueReference<TestStruct>);
    assert(isLvalueReference<TestStruct&>);
    assert(isLvalueReference<const TestStruct&>);

    assert(isRvalueReference<TestStruct&&>);
    assert(!isRvalueReference<TestStruct>);
}


void isSameTest()
{
    assert((isSame<const char *, const char *>));
    assert((!isSame<char, const char *>));
}


struct BaseStruct
{
    virtual ~BaseStruct() = default;
};

struct DerivedStruct : public BaseStruct
{
};

struct StandaloneStruct
{
};

void isBaseOfTest()
{
    constexpr bool yes = isBaseOf<BaseStruct, DerivedStruct>;
    constexpr bool no = !isBaseOf<BaseStruct, StandaloneStruct>;
    constexpr bool no2 = !isBaseOf<BaseStruct, int>;
    assert(yes);
    assert(no);
    assert(no2);
}


int main(int argc, const char ** args)
{
    isPointerTest();
    isReferenceTest();
    isSameTest();
    isBaseOfTest();
    return 0;
}
