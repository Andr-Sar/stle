#include "container/Vector.hpp"
#include "container/DynamicVector.hpp"

#include "Assert.hpp"


using namespace stle;


void vector_test()
{
    // Basic test
    allocator.reset_statistics();
    {
        Vector<int> vecEmpty{};
        assert(vecEmpty.isEmpty());

        Vector<int> vec{1, 2, 3};

        assert(vec.isNotEmpty());
        assert(vec[0] == 1);
        assert(vec[1] == 2);
        assert(vec[2] == 3);
        assert(vec.getFront() == 1);
        assert(vec.getBack() == 3);

        assert(vec.getSize() == 3);
    }

    auto allocatorStatistics = allocator.getStatistics();

    assert(allocatorStatistics.allocationsCount == 3);
    assert(allocatorStatistics.deallocationsCount == 3);

    // Copy
    {
        Vector<int> vec{1, 2, 3};
        Vector<int> vecCopy = vec;
        vec[0] = 10;

        assert(vec[0] == 10);
        assert(vecCopy[0] == 1);
        assert(vecCopy[1] == 2);
        assert(vecCopy[2] == 3);
        assert(vecCopy.getFront() == 1);
        assert(vecCopy.getBack() == 3);
        assert(vecCopy.getSize() == 3);
    }

    allocatorStatistics = allocator.getStatistics();

    assert(allocatorStatistics.allocationsCount == 9);
    assert(allocatorStatistics.deallocationsCount == 9);

    // Move
    {
        Vector<int> vec{1, 2, 3};
        Vector<int> vecMoved = move(vec);

        assert(vec.isEmpty());
        assert(vecMoved.isNotEmpty());
        assert(vecMoved[0] == 1);
        assert(vecMoved[1] == 2);
        assert(vecMoved[2] == 3);
        assert(vecMoved.getFront() == 1);
        assert(vecMoved.getBack() == 3);
        assert(vecMoved.getSize() == 3);

        auto concatenation = concat(vec, vecMoved);
    }
}


void dynamic_vector_test()
{
    // Basic test
    allocator.reset_statistics();
    {
        DynamicVector<int> vecEmpty{};
        assert(vecEmpty.isEmpty());

        DynamicVector<int> vec{1, 2, 3};

        assert(vec.isNotEmpty());
        assert(vec[0] == 1);
        assert(vec[1] == 2);
        assert(vec[2] == 3);
        assert(vec.getFront() == 1);
        assert(vec.getBack() == 3);

        assert(vec.getSize() == 3);
        assert(vec.getCapacity() == 3);
    }

    auto allocatorStatistics = allocator.getStatistics();

    assert(allocatorStatistics.allocationsCount == 3);
    assert(allocatorStatistics.deallocationsCount == 3);
}


int main(int argc, const char ** args)
{
    vector_test();
    dynamic_vector_test();
    return 0;
}
