#include "memory/Copy.hpp"
#include "memory/Move.hpp"
#include "memory/UniquePointer.hpp"

#include "Assert.hpp"
#include <cstdlib>
#include <cstring>
#include <cwchar>
#include <cuchar>
#include <iostream>


using namespace stle;

struct TestStruct
{
    int a;
    char b;
};


void memoryCopyTest()
{
    {
        const char *str = "This is simple string";
        auto strCopy = static_cast<char*>(malloc(strlen(str) + 1));
        memory::copy(str, strCopy, strlen(str) + 1);
        assert(strcmp(str, strCopy) == 0);
        free(strCopy);
    }

    {
        const char *str = "Small";
        auto strCopy = static_cast<char*>(malloc(strlen(str)));
        memory::copy(str, strCopy, strlen(str));
        assert(strcmp(str, strCopy) == 0);
        free(strCopy);
    }

    {
        const short arr[15] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        auto arrCopy = static_cast<short *>(malloc(15 * sizeof(short)));
        memory::copy(arr, arrCopy, 15);
        assert(arrCopy[0] == 1);
        assert(arrCopy[10] == 11);
        assert(arrCopy[14] == 15);
        free(arrCopy);
    }

    {
        const int arr[15] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        auto arrCopy = static_cast<int *>(malloc(15 * sizeof(int)));
        memory::copy(arr, arrCopy, 15);
        assert(arrCopy[0] == 1);
        assert(arrCopy[10] == 11);
        assert(arrCopy[14] == 15);
        free(arrCopy);
    }

    {
        const TestStruct arr[7] = {{1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}, {6, 6}, {7, 7}};
        auto arrCopy = static_cast<TestStruct *>(malloc(7 * sizeof(TestStruct)));
        memory::copy(arr, arrCopy, 7);
        assert(arrCopy[0].a == 1 && arrCopy[0].b == 1);
        assert(arrCopy[4].a == 5 && arrCopy[4].b == 5);
        assert(arrCopy[6].a == 7 && arrCopy[6].b == 7);
        free(arrCopy);
    }
}


void memoryMoveTest()
{
    const char *str = "StringString";
    const Size len = strlen(str) + 1;

    {
        auto strCopy = static_cast<char*>(malloc(len));
        memory::copy(str, strCopy, len);

        assert(strcmp(str, strCopy) == 0);
        memory::move(strCopy, strCopy + 1, 3);

        assert(strcmp(strCopy, "SStrngString") == 0);

        free(strCopy);
    }

    {
        auto strCopy = static_cast<char*>(malloc(len));
        memory::copy(str, strCopy, len);

        memory::move(strCopy, strCopy + 1, 2);

        assert(strcmp(strCopy, "SStingString") == 0);

        free(strCopy);
    }

    {
        auto strCopy = static_cast<char*>(malloc(len));
        memory::copy(str, strCopy, len);

        memory::move(strCopy + 1, strCopy, 2);

        assert(strcmp(strCopy, "trringString") == 0);

        free(strCopy);
    }
}


void uniquePointerTest()
{
    {
        auto simpleIntPtr = makeUnique<int>(100500);
        assert(*simpleIntPtr == 100500);
    }

    memory::AllocatorStatistics allocatorStatistics = memory::getDefaultAllocator().getStatistics();
    assert(allocatorStatistics.allocationsCount == allocatorStatistics.deallocationsCount);

    {
        auto simpleIntPtr_1 = makeUnique<int>(100500);
        UniquePointer<int> simpleIntPtr_2{};
        simpleIntPtr_2 = move(simpleIntPtr_1);
        assert(*simpleIntPtr_2 == 100500);
        assert(simpleIntPtr_1 == nullptr);
        assert(!simpleIntPtr_1);
        simpleIntPtr_2.reset(new int{100501});
        assert(*simpleIntPtr_2 == 100501);
        simpleIntPtr_2.reset();
        assert(simpleIntPtr_2 == nullptr);
    }
}


int main(int argc, char ** args)
{
    memoryCopyTest();
    memoryMoveTest();
    uniquePointerTest();
    return 0;
}
