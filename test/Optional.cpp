#include "Assert.hpp"
#include "Optional.hpp"

using namespace stle;


struct NonLiteralType
{
    int value = 0;
    NonLiteralType() = default;

    NonLiteralType(int val) : value(val)
    {
    }

    NonLiteralType(const NonLiteralType&) = delete;
    NonLiteralType(NonLiteralType &&another) noexcept:
        value(another.value)
    {
    }

    NonLiteralType & operator=(const NonLiteralType &another) = delete;
    NonLiteralType & operator=(NonLiteralType &&another) noexcept
    {
        value = another.value;
        return *this;
    }

    virtual ~NonLiteralType() = default;
};


void test()
{
    {
        constexpr Optional<int> optional{};
        constexpr Optional<int> anotherOptional = optional;
        assert(!anotherOptional);

        constexpr Optional<int> validOptional{100};
        constexpr auto validOptionalCopy = validOptional;

        assert(validOptionalCopy);
        assert((validOptionalCopy.value() == 100));

        try
        {
            auto val = optional.value();
            assert(val == 0);
            assert(false);
        }
        catch (BadOptionalAccess &e)
        {
            assert(true);
        }
    }

    {
        Optional<NonLiteralType> optional{NonLiteralType{100}};
        Optional<NonLiteralType> anotherOptional{};

        anotherOptional = move(optional);
        assert(anotherOptional);
        assert((anotherOptional.value().value == 100));
    }

    {
        Optional<NonLiteralType> optional{inplace, 100};
        assert((optional.value().value == 100));
    }

    {
        auto optional = makeOptional(NonLiteralType{100});
        assert((optional.value().value == 100));
    }
}


int main(int argc, const char ** args)
{
    test();
    return 0;
}
