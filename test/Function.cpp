#include "Assert.hpp"

#include "Function.hpp"


using namespace stle;


int sum(int a, int b)
{
    return a + b;
}


void test()
{
    auto f1 = function( [](int a, int b) -> int {return a + b;} );
    auto f2 = function(sum);
    assert(f1.callWith(1, 2) == 3);
    assert(f2(3, 4) == 7);

    auto dcArg = deferredCall([](int a) -> int {return a;}, 2);
    auto dc = deferredCall([](int a, int b) -> int {return a + b;}, 1, dcArg);
    assert(dc() == 3);
}


int main(int argc, const char ** args)
{
    test();
    return 0;
}
