#include "Assert.hpp"

#include "Tuple.hpp"
#include "type_traits/IsSame.hpp"

#include <iostream>


using namespace stle;


void test()
{
    {
        constexpr auto t = tuple(100500, true, 'T', "Tuple");
        Size s = sizeof(t);
        assert(t.head == 100500);
        assert(t.rest.head == true);
        assert(t.rest.rest.head == 'T');
        auto v0 = t.at<0>();
        assert(v0 == 100500);
        assert(t.at<1>() == true);
        assert(t.at<2>() == 'T');
        assert(t.at<2>() == 'T');

        auto func = [](int num, bool flag, char ch, const char *text)
        {
            assert(num == 100500);
            assert(flag == true);
            assert(ch == 'T');
        };

        t.expandTo(func);
    }

    {
        auto t = tuple(100500, true, 'T');
        t.at<0>() = 200500;
        assert(t.first() == 200500);
        assert(t.last() == 'T');
    }

    {
        auto t = tuple(100500);
        assert(t.first() == 100500);
        assert(t.last() == 100500);
    }

    {
        auto t = Tuple<int, int, int, int>(1, 2, 3, 4);
        auto size = sizeof(t);
    }
}


void concatTest()
{
    {
        constexpr auto a = tuple(1.0, 'A');
        constexpr auto b = tuple(2.0, 'B', true);
        constexpr auto c = concat(a, b);

        assert(c.at<0>() == 1.0);
        assert(c.at<1>() == 'A');
        assert(c.at<2>() == 2.0);
        assert(c.at<3>() == 'B');
        assert(c.at<4>() == true);
    }

    {
        auto a = tuple(1.0, 'A');
        auto c = concat(a, tuple(2.0, 'B', true));

        assert(c.at<0>() == 1.0);
        assert(c.at<1>() == 'A');
        assert(c.at<2>() == 2.0);
        assert(c.at<3>() == 'B');
        assert(c.at<4>() == true);
    }
}


void mapTest()
{
    auto a = tuple(2.0, 'B', true);
    auto b = map(a, [](double val, char ch, bool flag) {
        return tuple(val + 1.0, 'C', !flag);
    });

    assert(b.at<0>() == 3.0);
    assert(b.at<1>() == 'C');
    assert(b.at<2>() == false);

}


void foldTest()
{
    {
        auto a = tuple(1, 2, 3, 4, 5);
        auto sum = fold(a, [](int accumulated, int val) { return accumulated + val;}, 0);
        assert(sum == 1 + 2 + 3 + 4 + 5);
    }

    {
        auto a = tuple(1, 2, 3, 4, 5);
        auto sum = foldl(a, [](int accumulated, int val) { return accumulated + val;}, 0);
        assert(sum == 1 + 2 + 3 + 4 + 5);
    }
}


int main(int argc, const char ** args)
{
    test();
    concatTest();
    mapTest();
    foldTest();
    return 0;
}
