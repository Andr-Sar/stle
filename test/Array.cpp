#include "Assert.hpp"
#include "Array.hpp"

using namespace stle;


void array_test()
{
    // Basic test
    Array<int, 8> emptyArray{};
    assert(emptyArray.isEmpty());

    constexpr Array<int, 4> array = {1, 2, 3};
    assert(array.getSize() == 3);
    assert(array[0] == 1);
    assert(array[1] == 2);
    assert(array[2] == 3);
    assert(array.getFirst() == 1);
    assert(array.getLast() == 3);

    /*for (int a : array)
    {

    }*/

    constexpr auto iof = array.indexOf(3);
    assert(iof.value() == 2);

    {
        Array<int, 4> arr1 = {1, 2, 3};
        Array<int, 4> arr2 = {4, 5, 6};

        arr1 = arr2;
        assert(arr1[0] == 4);
        assert(arr1[1] == 5);
        assert(arr1[2] == 6);
    }

    {
        Array<int, 4> arr1 = {4, 5, 6, 7};
        Array<int, 3> arr2{};

        copy(arr1, arr2);
        assert(arr2[0] == 4);
        assert(arr2[1] == 5);
        assert(arr2[2] == 6);
    }
}


int main(int argc, const char ** args)
{
    array_test();
    return 0;
}
