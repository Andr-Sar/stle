#include "../include/Allocator.hpp"
#include "../include/memory/StandardAllocator.hpp"


namespace stle
{

Allocator::Allocator(): defaultAllocator(true)
{
    interface = new memory::StandardAllocator{};
}

Allocator::~Allocator()
{
    if (defaultAllocator)
    {
        delete interface;
    }
    else
    {
        interface = nullptr;
    }
}

Allocator allocator = Allocator();

}
