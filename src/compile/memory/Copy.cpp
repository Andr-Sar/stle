#include "../include/memory/Copy.hpp"


namespace stle
{
namespace memory
{

void copyRaw(const void *srcPtr, void *destPtr, Size nbytes) noexcept
{
    auto src = static_cast<const char *>(srcPtr);
    auto dest = static_cast<char *>(destPtr);

    const Size n = nbytes / sizeof(Size);
    const Size howMuchLeft = nbytes - (n * sizeof(Size));

    for (Size i = 0; i < n; ++i)
    {
        *(Size*)dest = *(const Size*)src;
        src += sizeof(Size);
        dest += sizeof(Size);
    }

    for (Size i = 0; i < howMuchLeft; ++i)
    {
        *dest = *src;
        ++dest;
        ++src;
    }
}


template<>
void copy(const char *src, char *dest, Size count)
{
    copyRaw(src, dest, count);
}

template<>
void copy(const unsigned char *src, unsigned char *dest, Size count)
{
    copyRaw(src, dest, count);
}

template<>
void copy(const short *src, short *dest, Size count)
{
    copyRaw(src, dest, count * sizeof(short));
}

template<>
void copy(const unsigned short *src, unsigned short *dest, Size count)
{
    copyRaw(src, dest, count * sizeof(unsigned short));
}

template<>
void copy(const char16_t *src, char16_t *dest, Size count)
{
    copyRaw(src, dest, count * sizeof(char16_t));
}

}
}
