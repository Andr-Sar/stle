#include "../include/memory/StandardAllocator.hpp"

#include <cstdlib>
#include <memory/StandardAllocator.hpp>


namespace stle
{
namespace memory
{

void *StandardAllocator::allocate(Size sizeOfType)
{
    void *data = malloc(sizeOfType);

    if (data == nullptr)
    {
        throw AllocationError{};
    }

    statistics.allocatedBytes += sizeOfType;
    ++statistics.allocationsCount;

    return data;
}

void *StandardAllocator::allocate(Size sizeOfType, Size count)
{
    const Size bytesCount = count * sizeOfType;
    void *data = malloc(bytesCount);

    if (data == nullptr)
    {
        throw AllocationError{};
    }

    statistics.allocatedBytes += bytesCount;
    statistics.allocationsCount += count;

    return data;
}

void *StandardAllocator::reallocate(void *ptr, Size sizeOfType, Size oldCount, Size newCount)
{
    const Size bytesCount = newCount * sizeOfType;
    void *data = realloc(ptr, bytesCount);

    if (data == nullptr)
    {
        throw AllocationError{};
    }

    statistics.allocationsCount += newCount - oldCount;
    statistics.allocatedBytes += bytesCount - (oldCount * sizeOfType);

    return data;
}

void StandardAllocator::free(void *ptr, Size sizeOfType)
{
    ::free(ptr);

    statistics.deallocatedBytes += sizeOfType;
    statistics.deallocationsCount += 1;
}

void StandardAllocator::free(void *ptr, Size sizeOfType, Size count)
{
    ::free(ptr);

    statistics.deallocatedBytes += count * sizeOfType;
    statistics.deallocationsCount += count;
}

void *StandardAllocator::allocateString(Size size)
{
    return allocate(size);
}

void StandardAllocator::deallocateString(void *ptr, Size size)
{
    free(ptr, size);
}

AllocatorStatistics StandardAllocator::getStatistics() const
{
    return statistics;
}

void StandardAllocator::resetStatistics()
{
    statistics = AllocatorStatistics{};
}

StandardAllocator::~StandardAllocator()
{
}

}
}
