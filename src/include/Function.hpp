#pragma once

#include "Forward.hpp"
//#include "Tuple.hpp"
//#include <functional>


namespace stle
{

template <class Func>
class Function
{
public:
    explicit Function(Func &&func):
        f(forward<Func>(func))
    {
    }

    template <class ... Args>
    decltype(auto) operator()(Args &&...args) const
    {
        return f(forward<Args>(args)...);
    }

    template <class ... Args>
    decltype(auto) callWith(Args &&...args) const
    {
        return f(forward<Args>(args)...);
    }

private:
    Func f;
};

template <class Func>
Function<Func> function(Func &&func)
{
    return Function<Func>{forward<Func>(func)};
}


template <class T>
class UnaryPredicate
{
public:
    virtual bool operator()(const T &obj) = 0;
    virtual ~UnaryPredicate() = default;
};

template <class T, class Y>
class BinaryPredicate
{
public:
    virtual bool operator()(const T &, const Y &) = 0;
    virtual ~BinaryPredicate() = default;
};

template <class T>
class UnaryVisitor
{
public:
    virtual void operator()(T &obj) = 0;
    virtual ~UnaryVisitor() = default;
};


template <class Func, class T>
class UnaryPredicateLambda : public UnaryPredicate<T>
{
public:
    UnaryPredicateLambda(Func &&func):
            f(forward<Func>(func))
    {
    }

    bool operator()(const T &obj) override
    {
        return f(obj);
    }

    ~UnaryPredicateLambda() override = default;

private:
    Func f;
};

template <class Func, class T, class Y>
class BinaryPredicateLambda : public BinaryPredicate<T, Y>
{
public:
    BinaryPredicateLambda(Func &&func):
            f(forward<Func>(func))
    {
    }

    bool operator()(const T &a, const T &b) override
    {
        return f(a, b);
    }

    ~BinaryPredicateLambda() override = default;

private:
    Func f;
};

template <class Func, class T>
class UnaryVisitorLambda : public UnaryVisitor<T>
{
public:
    UnaryVisitorLambda(Func &&func):
            f(forward<Func>(func))
    {
    }

    void operator()(T &obj) override
    {
        f(obj);
    }

    ~UnaryVisitorLambda() override = default;

private:
    Func f;
};


template <class Result>
class DeferredCall
{
public:
    virtual Result operator()() = 0;
    virtual ~DeferredCall() = default;
};

template <class Func, class Result, class ... Args>
class DeferredCallTemplate;


/*template <class T>
decltype(auto) _passArg(T &&arg)
{
    return forward<T>(arg);
}

template <class Func, class Result, class ... Args>
decltype(auto) _passArg(DeferredCallTemplate<Func, Result, Args...> &&arg)
{
    return forward<DeferredCallTemplate<Func, Result, Args...>>(arg)();
}


template <class Func, class T, Size ...indices>
auto makeDeferredCallImpl(T &t, Func &&func, IndexSequence<indices...>)
{
    return forward<Func>(func)(_passArg(t.template at<indices>())...);
}*/


/*template <class Func, class Result, class ... Args>
class DeferredCallTemplate : public DeferredCall<Result>
{
public:
    DeferredCallTemplate(Func &&func, Tuple<Args...> &&args):
        func(forward<Func>(func)),
        args(move(args))
    {
    }

    Result operator()() override
    {
        return args.expandTo(forward<Func>(func));
        //return makeDeferredCallImpl(args, forward<Func>(func), typename MakeIndexSequence<sizeof...(Args)>::Type{});
    }

    operator Result()
    {
        return this->operator()();
    }

    ~DeferredCallTemplate() override = default;

private:
    Func func;
    Tuple<Args...> args;
};


template <class Func, class ... Args>
auto deferredCall(Func &&func, Args &&...args)
{
    using ResultType = decltype(func(forward<Args>(args)...));
    return DeferredCallTemplate<Func, ResultType, Args...>{forward<Func>(func), tuple(forward<Args>(args)...)};
}*/


}
