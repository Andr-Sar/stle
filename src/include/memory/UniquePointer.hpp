#pragma once

#include "../Allocator.hpp"
#include "../Forward.hpp"


namespace stle
{

template <class T>
void defaultUniquePointerDeleter(T *ptr)
{
    ptr->~T();
    allocator.free(ptr);
};


template <class T, void(*deleterFunction)(T *) = &defaultUniquePointerDeleter<T>>
class UniquePointer
{
public:
    UniquePointer() noexcept: pointer(nullptr)
    {
    }

    explicit UniquePointer(T * const pointer) noexcept:
        pointer(pointer)
    {
    }

    UniquePointer(const UniquePointer &another) = delete;

    template <class U>
    explicit UniquePointer(UniquePointer<U, deleterFunction> &&another) noexcept : pointer(another.pointer)
    {
        another.pointer = nullptr;
    }

    UniquePointer &operator=(const UniquePointer &another) = delete;

    template <class U>
    UniquePointer &operator=(UniquePointer<U, deleterFunction> &&another) noexcept
    {
        this->reset(another.release());
        return *this;
    }

    T *get() const noexcept
    {
        return pointer;
    }

    explicit operator bool() const noexcept
    {
        return pointer != nullptr;
    }

    bool operator==(decltype(nullptr) ptr) const noexcept
    {
        return pointer == nullptr;
    }

    T *operator->() const
    {
        return pointer;
    }

    T &operator*() const
    {
        return *pointer;
    }

    void reset(T *ptr = nullptr)
    {
        free();
        pointer = ptr;
    }

    T *release() noexcept
    {
        T *result = pointer;
        pointer = nullptr;
        return result;
    }

    UniquePointer &operator=(decltype(nullptr) ptr) noexcept
    {
        free();
        pointer = nullptr;
    }

    ~UniquePointer()
    {
        free();
    }

private:
    void free()
    {
        if (*this)
        {
            deleterFunction(pointer);
        }
    }

private:
    T *pointer;
};


template <class T, void(*deleterFunction)(T *), class ... Args>
UniquePointer<T, deleterFunction> makeUnique(Args&&... args)
{
    T *ptr = allocator.allocate<T>();
    new (ptr) T(forward<Args>(args)...);
    return UniquePointer<T, deleterFunction>{ptr};
}

template <class T, class ... Args>
auto makeUnique(Args&&... args)
{
    return makeUnique<T, &defaultUniquePointerDeleter<T>>(forward<Args>(args)...);
}

}
