#pragma once

#include "../BasicTypes.hpp"
#include "AllocationError.hpp"
#include "AllocatorStatistics.hpp"


namespace stle
{
namespace memory
{

class AllocatorInterface
{
public:
    virtual void *allocate(Size size) = 0;
    virtual void *allocate(Size size, Size count) = 0;
    virtual void *reallocate(void *ptr, Size size, Size oldCount, Size newCount) = 0;
    virtual void free(void *ptr, Size size) = 0;
    virtual void free(void *ptr, Size size, Size count) = 0;
    virtual void *allocateString(Size size) = 0;
    virtual void deallocateString(void *ptr, Size size) = 0;
    virtual AllocatorStatistics getStatistics() const = 0;
    virtual void resetStatistics() = 0;
    virtual ~AllocatorInterface() = default;
};

}
}
