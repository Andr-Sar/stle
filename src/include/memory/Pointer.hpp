#pragma once


namespace stle
{

template <class T, class ControlBlock = >
class Pointer
{
    using Self = Pointer;

public:

private:
    T *pointer;
    T *controlBlock;
};

}
