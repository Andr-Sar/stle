#pragma once

#include "../Exception.hpp"


namespace stle
{
namespace memory
{

class AllocationError : public Exception
{
public:
    AllocationError() noexcept
    {
    }

    const char *what() const noexcept override
    {
        return "Allocation error occurred";
    }

    ~AllocationError() override = default;
};

}
}