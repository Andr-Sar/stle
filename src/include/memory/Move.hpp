#pragma once

#include "../BasicTypes.hpp"
#include "../Move.hpp"


namespace stle
{
namespace memory
{

template <class T>
void move(T *src, T *dest, Size count)
{
    const Size howMuchLeft = count % 2;
    const Size n = count - howMuchLeft;

    if (dest < src)
    {
        for (Size i = 0; i < n; i += 2)
        {
            dest[i] = stle::move(src[i]);
            dest[i + 1] = stle::move(src[i + 1]);
        }

        if (howMuchLeft > 0)
        {
            dest[n] = stle::move(src[n]);
        }
    }
    else
    {
        for (int i = static_cast<int>(count); i > howMuchLeft; i -= 2)
        {
            dest[i - 1] = stle::move(src[i - 1]);
            dest[i - 2] = stle::move(src[i - 2]);
        }

        if (howMuchLeft > 0)
        {
            dest[0] = stle::move(src[0]);
        }
    }
}

//template<>
//void move<char>(const char *src, char *dest, Size count);
//
//template<>
//void move<unsigned char>(const unsigned char *src, unsigned char *dest, Size count);
//
//template<>
//void move<short>(const short *src, short *dest, Size count);
//
//template<>
//void move<unsigned short>(const unsigned short *src, unsigned short *dest, Size count);
//
//template<>
//void move<char16_t>(const char16_t *src, char16_t *dest, Size count);

}
}