#pragma once

#include "../BasicTypes.hpp"


namespace stle
{
namespace memory
{

template <class T>
void copy(const T *src, T *dest, Size count)
{
    const Size howMuchLeft = count % 2;
    const Size n = count - howMuchLeft;

    for (Size i = 0; i < n; i += 2)
    {
        dest[i] = src[i];
        dest[i + 1] = src[i + 1];
    }

    if (howMuchLeft > 0)
    {
        dest[n] = src[n];
    }
}

template<>
void copy<char>(const char *src, char *dest, Size count);

template<>
void copy<unsigned char>(const unsigned char *src, unsigned char *dest, Size count);

template<>
void copy<short>(const short *src, short *dest, Size count);

template<>
void copy<unsigned short>(const unsigned short *src, unsigned short *dest, Size count);

template<>
void copy<char16_t>(const char16_t *src, char16_t *dest, Size count);

}
}
