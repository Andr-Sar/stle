#pragma once

#include "../BasicTypes.hpp"


namespace stle
{
namespace memory
{

struct AllocatorStatistics
{
    AllocatorStatistics():
            availableBytes(0),
            allocatedBytes(0),
            deallocatedBytes(0),
            allocationsCount(0),
            deallocationsCount(0)
    {
    }

    Size availableBytes;
    Size allocatedBytes;
    Size deallocatedBytes;
    Size allocationsCount;
    Size deallocationsCount;

    Size getUsedBytes() const
    {
        return allocatedBytes - deallocatedBytes;
    }

    Size getCurrentAllocationsCount() const
    {
        return deallocationsCount - allocationsCount;
    }
};

}
}
