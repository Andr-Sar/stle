#pragma once

#include "AllocatorInterface.hpp"


namespace stle
{
namespace memory
{

class StandardAllocator : public AllocatorInterface
{
public:
    void *allocate(Size sizeOfType) override;
    void *allocate(Size sizeOfType, Size count) override;
    void *reallocate(void *ptr,  Size sizeOfType, Size oldCount, Size newCount) override;
    void free(void *ptr, Size sizeOfType) override;
    void free(void *ptr, Size sizeOfType, Size count) override;
    void *allocateString(Size size) override;
    void deallocateString(void *ptr, Size size) override;
    AllocatorStatistics getStatistics() const override;
    void resetStatistics() override;
    ~StandardAllocator() override;

private:
    AllocatorStatistics statistics;
};

}
}
