#pragma once

#include "../BasicTypes.hpp"
#include "../type_traits/IsScalar.hpp"


namespace stle
{
namespace memory
{

namespace
{
    template <class T>
    void callDestructors(T *data, Size count)
    {
        for (Size i = 0; i < count; ++i)
        {
            data[i].~T();
        }
    }
}


template <class T>
void clear(T *data, Size count)
{
    if (!isScalar<T>)
    {
        callDestructors(data, count);
    }
}

}
}
