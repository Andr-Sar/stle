#pragma once

#include <iostream>


namespace stle
{

inline void _assert(const char *expr, const char *file, int line)
{
    std::cout << "Assert failed: '" << expr << "' at line " << line << " in file " << file << std::endl;
}


#define assert(_Expression) \
    (void) \
    ((!!(_Expression)) || \
    (_assert(#_Expression,__FILE__,__LINE__),0))

}
