#pragma once

#include "../Function.hpp"


namespace stle
{
namespace fp
{

template <class Func>
class Function
{
public:
    explicit Function(Func &&func):
            f(forward<Func>(func))
    {
    }

    template <class ... Args>
    auto operator()(Args &&...args)
    {
        return deferredCall(forward<Func>(f), forward<Args>(args)...);
    }

private:
    Func f;
};

template <class Func>
Function<Func> function(Func &&func)
{
    return Function<Func>{forward<Func>(func)};
}

///

template <class P, class F>
struct MatchCase
{
    P predicate;
    F function;
};

template <class Predicate>
struct MatchPredicate
{
    explicit MatchPredicate(Predicate &&predicate):
            predicate(forward<Predicate>(predicate))
    {

    }

    template <class Function>
    MatchCase<Predicate, Function> operator>>(Function &&function) const
    {
        return {predicate, forward<Function>(function)};
    }

    Predicate predicate;
};

template <class Predicate>
auto makeMatchPredicate(Predicate &&predicate)
{
    return MatchPredicate<Predicate>(forward<Predicate>(predicate));
};

template <class T>
auto eq(T val)
{
    return makeMatchPredicate(
        [val](const auto &arg) -> bool { return val == arg; }
    );
}



/*template <class T, class ... Patterns>
auto match(T &&val, Patterns &&...pat)
{

};*/


}
}
