#pragma once

#include "None.hpp"
#include "BasicTypes.hpp"


namespace stle
{

template <int n, int current, class ... Args>
struct _GetTypeByIndex
{
    using Type = typename _GetTypeByIndex<n, current + 1, Args...>::Type;
};

template <int n, int current, class Arg, class ... Args>
struct _GetTypeByIndex<n, current, Arg, Args...>
{
    using Type = typename _GetTypeByIndex<n, current + 1, Args...>::Type;
};

template <int n, class Arg, class ... Args>
struct _GetTypeByIndex<n, n, Arg, Args...>
{
    using Type = Arg;
};

template <int n, int current>
struct _GetTypeByIndex<n, current>
{
    using Type = None;
};


template <int n, class ... Args>
struct GetTypeByIndex
{
    using Type = typename _GetTypeByIndex<n, 0, Args...>::Type;
};

template <int n>
struct GetTypeByIndex<n>
{
    using Type = None;
};


template <class T, class ... Args>
struct HasType
{
    static constexpr bool value = HasType<T, Args...>::value;
};

template <class T, class Arg, class ... Args>
struct HasType<T, Arg, Args...>
{
    static constexpr bool value = HasType<T, Args...>::value;
};

template <class T, class ... Args>
struct HasType<T, T, Args...>
{
    static constexpr bool value = true;
};

template <class T>
struct HasType<T>
{
    static constexpr bool value = false;
};


// TypeList

template <class ... Args>
struct TypeList
{
    static constexpr Size size = sizeof...(Args);
};

template <class Arg, class ... Args>
struct TypeList<Arg, Args...>
{
    using Head = Arg;
    using Rest = TypeList<Args...>;

    static constexpr Size size = sizeof...(Args) + 1;

    template <Size i>
    struct At
    {
        using Type = typename GetTypeByIndex<i, Arg, Args...>::Type;
    };

    template <class T>
    struct Has
    {
        static constexpr bool value = HasType<T, Arg, Args...>::value;
    };

    template <class ... AnotherArgs>
    struct Append
    {
        using Type = TypeList<Arg, Args..., AnotherArgs...>;
    };
};

template <class Arg>
struct TypeList<Arg>
{
    using Head = Arg;
    using Rest = None;

    static constexpr Size size = 1;

    template <Size i>
    struct At
    {
        using Type = typename GetTypeByIndex<i, Arg>::Type;
    };

    template <class T>
    struct Has
    {
        static constexpr bool value = HasType<T, Arg>::value;
    };

    template <class ... AnotherArgs>
    struct Append
    {
        using Type = TypeList<Arg, AnotherArgs...>;
    };
};

template <>
struct TypeList<>
{
    using Head = None;
    using Rest = None;

    static constexpr Size size = 0;

    template <Size i>
    struct At
    {
        using Type = None;
    };

    template <class T>
    struct Has
    {
        static constexpr bool value = false;
    };

    template <class ... AnotherArgs>
    struct Append
    {
        using Type = TypeList<AnotherArgs...>;
    };
};


using EmptyTypeList = TypeList<>;

}
