#pragma once

#include "BasicTypes.hpp"


namespace stle
{

template <Size ... seq>
struct IndexSequence
{
};

template <Size n>
struct MakeIndexSequence
{
    using Type = IndexSequence<>;
};

template <> struct MakeIndexSequence<0>  { using Type = IndexSequence<>; };
template <> struct MakeIndexSequence<1>  { using Type = IndexSequence<0>; };
template <> struct MakeIndexSequence<2>  { using Type = IndexSequence<0, 1>; };
template <> struct MakeIndexSequence<3>  { using Type = IndexSequence<0, 1, 2>; };
template <> struct MakeIndexSequence<4>  { using Type = IndexSequence<0, 1, 2, 3>; };
template <> struct MakeIndexSequence<5>  { using Type = IndexSequence<0, 1, 2, 3, 4>; };
template <> struct MakeIndexSequence<6>  { using Type = IndexSequence<0, 1, 2, 3, 4, 5>; };
template <> struct MakeIndexSequence<7>  { using Type = IndexSequence<0, 1, 2, 3, 4, 5, 6>; };
template <> struct MakeIndexSequence<8>  { using Type = IndexSequence<0, 1, 2, 3, 4, 5, 6, 7>; };
template <> struct MakeIndexSequence<9>  { using Type = IndexSequence<0, 1, 2, 3, 4, 5, 6, 7, 8>; };
template <> struct MakeIndexSequence<10> { using Type = IndexSequence<0, 1, 2, 3, 4, 5, 6, 7, 8, 9>; };
template <> struct MakeIndexSequence<11> { using Type = IndexSequence<0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10>; };
template <> struct MakeIndexSequence<12> { using Type = IndexSequence<0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11>; };
template <> struct MakeIndexSequence<13> { using Type = IndexSequence<0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12>; };
template <> struct MakeIndexSequence<14> { using Type = IndexSequence<0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13>; };
template <> struct MakeIndexSequence<15> { using Type = IndexSequence<0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14>; };
template <> struct MakeIndexSequence<16> { using Type = IndexSequence<0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15>; };

}
