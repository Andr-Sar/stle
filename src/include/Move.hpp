#pragma once

#include "type_traits/RemoveReference.hpp"


namespace stle
{

template <typename T>
constexpr typename RemoveReference<T>::Type&& move(T&& arg)
{
    return static_cast<typename RemoveReference<T>::Type&&>(arg);
}

}
