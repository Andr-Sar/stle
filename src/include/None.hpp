#pragma once


// Used to represent empty (last, missing) type in various recursive structures and algorithms.


namespace stle
{

struct None
{
};

}
