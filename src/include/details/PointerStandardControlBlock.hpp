#pragma once


namespace stle
{
namespace detail
{

struct PointerStandardControlBlock
{
    PointerStandardControlBlock():
            sharedCounter(0),
            weakCounter(0)
    {
    }

    unsigned int sharedCounter;
    unsigned int weakCounter;
};

}
}
