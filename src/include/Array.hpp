#pragma once

// Simple array class which does not depend on anything

#include "BasicTypes.hpp"
#include "type_traits/IsLiteralType.hpp"
#include "Optional.hpp"
#include "Forward.hpp"
#include "Move.hpp"
#include "memory/Copy.hpp"
#include "memory/Move.hpp"


namespace stle
{

template <class T, Size capacity>
class Array;

///

namespace details
{

template <class T, Size capacity, bool _isLiteralType = isLiteralType<T> >
struct ArrayImplementation
{
    Size size;
    T array[capacity];

    constexpr ArrayImplementation() noexcept: size(0)
    {
    }

    template <class Arg, class ...Args>
    constexpr explicit ArrayImplementation(Arg &&arg, Args &&...args):
        size(1 + sizeof...(Args)),
        array{forward<Arg>(arg), forward<Args>(args)...}
    {
    }

    explicit ArrayImplementation(const T * const arr): size(capacity)
    {
        for (Size i = 0; i < capacity; ++i)
        {
            array[i] = arr[i];
        }
    }

    ArrayImplementation(const ArrayImplementation &another): size(another.size)
    {
        for (Size i = 0; i < size; ++i)
        {
            array[i] = another.array[i];
        }
    }

    ArrayImplementation(ArrayImplementation &&another) noexcept: size(another.size)
    {
        for (Size i = 0; i < size; ++i)
        {
            array[i] = move(another.array[i]);
        }
    }

    ArrayImplementation &operator=(const ArrayImplementation &another)
    {
        for (Size i = 0; i < another.size; ++i)
        {
            array[i] = another.array[i];
        }

        size = another.size;

        return *this;
    }

    ArrayImplementation &operator=(ArrayImplementation &&another) noexcept
    {
        for (Size i = 0; i < another.size; ++i)
        {
            array[i] = move(another.array[i]);
        }

        size = another.size;

        return *this;
    }

    constexpr const T &operator[](const Size index) const
    {
        return array[index];
    }

    constexpr T &operator[](const Size index)
    {
        return array[index];
    }

    void clear()
    {
        size = 0;
    }

    void removeLast()
    {
        if (size > 0)
        {
            --size;
        }
    }
};


template <class T, Size capacity>
struct ArrayImplementation<T, capacity, false>
{
    Size size;
    T array[capacity];

    constexpr ArrayImplementation() noexcept: size(0)
    {
    }

    template <class Arg, class ...Args>
    constexpr explicit ArrayImplementation(Arg &&arg, Args &&...args):
        size(1 + sizeof...(Args)),
        array{forward<Arg>(arg), forward<Args>(args)...}
    {
    }

    explicit ArrayImplementation(const T * const arr): size(capacity)
    {
        for (Size i = 0; i < capacity; ++i)
        {
            array[i] = arr[i];
        }
    }

    ArrayImplementation(const ArrayImplementation &another): size(another.size)
    {
        for (Size i = 0; i < size; ++i)
        {
            array[i] = another.array[i];
        }
    }

    ArrayImplementation(ArrayImplementation &&another) noexcept: size(another.size)
    {
        for (Size i = 0; i < size; ++i)
        {
            array[i] = move(another.array[i]);
        }
    }

    ArrayImplementation &operator=(const ArrayImplementation &another)
    {
        for (Size i = 0; i < another.size; ++i)
        {
            array[i] = another.array[i];
        }

        size = another.size;

        return *this;
    }

    ArrayImplementation &operator=(ArrayImplementation &&another) noexcept
    {
        for (Size i = 0; i < another.size; ++i)
        {
            array[i] = move(another.array[i]);
        }

        size = another.size;

        return *this;
    }

    constexpr const T &operator[](const Size index) const
    {
        return array[index];
    }

    constexpr T &operator[](const Size index)
    {
        return array[index];
    }

    void clear()
    {
        for (Size i = 0; i < size; ++i)
        {
            array[i].~T();
        }

        size = 0;
    }

    void removeLast()
    {
        if (size > 0)
        {
            --size;
            array[size].~T();
        }
    }

    ~ArrayImplementation()
    {
        for (Size i = 0; i < size; ++i)
        {
            array[i].~T();
        }
    }
};

}

///

template <class T, Size capacity>
class Array
{
public:
    using StoredType = T;
    using Iterator = StoredType*;
    using ConstIterator = const StoredType*;

public:
    constexpr Array() noexcept = default;

    template <class Arg, class ...Args>
    constexpr Array(Arg &&arg, Args &&...args): array{forward<Arg>(arg), forward<Args>(args)...}
    {
    }

    explicit Array(const StoredType * const arr): array(arr)
    {
    }

    inline constexpr Size getSize() const noexcept
    {
        return array.size;
    }

    inline constexpr bool isEmpty() const noexcept
    {
        return getSize() == 0;
    }

    constexpr bool isNotEmpty() const noexcept
    {
        return getSize() > 0;
    }

    inline constexpr Size getCapacity() const noexcept
    {
        return capacity;
    }

    constexpr const StoredType &operator[](const Size index) const
    {
        return array[index];
    }

    constexpr StoredType &operator[](const Size index)
    {
        return array[index];
    }

    constexpr const T* data() const noexcept
    {
        return array.array;
    }

    constexpr T* data() noexcept
    {
        return array.array;
    }

    constexpr const StoredType &getFirst() const noexcept
    {
        return array[0];
    }

    constexpr const StoredType &getLast() const noexcept
    {
        return array[getSize() - 1];
    }

    constexpr StoredType &getFirst() noexcept
    {
        return array[0];
    }

    constexpr StoredType &getLast() noexcept
    {
        return array[getSize() - 1];
    }

    void pushBack(const StoredType &element)
    {
        array[getSize()] = element;
    }

    void removeLast()
    {
        array.removeLast();
    }

    Optional<StoredType> popLast()
    {
        Optional<StoredType> result;
        if (isNotEmpty())
        {
            --array.size;
            result = move(array[array.size]);
        }

        return move(result);
    }

    ~Array() = default;

    void clear()
    {
        array.clear();
    }

    constexpr Iterator begin() noexcept
    {
        return array.array;
    }

    constexpr ConstIterator begin() const noexcept
    {
        return array.array;
    }

    constexpr ConstIterator cbegin() const noexcept
    {
        return array.array;
    }

    constexpr Iterator end() noexcept
    {
        return array.array + getSize();
    }

    constexpr ConstIterator end() const noexcept
    {
        return array.array + getSize();
    }

    constexpr ConstIterator cend() const noexcept
    {
        return array.array + getSize();
    }

    Iterator find(const StoredType &value) noexcept
    {
        Iterator result = end();
        for (Size i = 0; i < getSize(); ++i)
        {
            if (array[i] == value)
            {
                result = array.array + i;
                break;
            }
        }

        return result;
    }

    ConstIterator find(const StoredType &value) const noexcept
    {
        Iterator result = cend();
        for (Size i = 0; i < getSize(); ++i)
        {
            if (array[i] == value)
            {
                result = array.array + i;
                break;
            }
        }

        return result;
    }

    constexpr Optional<Size> indexOf(const StoredType &value) const noexcept
    {
        return indexOfImpl(value, 0);
    }

private:
    constexpr Optional<Size> indexOfImpl(const StoredType &value, const Size index) const noexcept
    {
        return index < array.size ? (array[index] == value ? Optional<Size>{index} : indexOfImpl(value, index + 1))
                                  : nullopt;
    }

private:
    details::ArrayImplementation<T, capacity> array;
};


template <class Arg, class ...Args>
constexpr auto makeArray(Arg &&arg, Args &&...args)
{
    return Array<Arg, sizeof...(Args) + 1>{forward<Arg>(arg), forward<Args>(args)...};
}


template <class T, Size capacityA, Size capacityB>
void copy(const Array<T, capacityA> &from, Array<T, capacityB> &to)
{
    const Size n = to.getCapacity() >= from.getSize() ? from.getSize() : to.getCapacity();

    for (Size i = 0; i < n; ++i)
    {
        to[i] = from[i];
    }
}

template <class T, Size capacityA, Size capacityB>
void move(Array<T, capacityA> &&from, Array<T, capacityB> &to)
{
    const Size n = to.getCapacity() >= from.getSize() ? from.getSize() : to.getCapacity();

    for (Size i = 0; i < n; ++i)
    {
        to[i] = move(from[i]);
    }
}

}
