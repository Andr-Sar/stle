#pragma once

#include "Forward.hpp"
#include "None.hpp"
#include "TypeList.hpp"
#include "BasicTypes.hpp"


namespace stle
{

template <Size currentMempos, class T>
struct TupleTypePlacementInfo
{
    static constexpr Size position = currentMempos % sizeof(T) == 0 ? currentMempos : (currentMempos / 8) * 8 + 8;
    static constexpr Size gap = position - currentMempos;
    static constexpr Size newMempos = position + sizeof(T);
};

template <class T>
struct TupleTypePlacementInfo<0, T>
{
    static constexpr Size position = 0;
    static constexpr Size gap = 0;
    static constexpr Size newMempos = sizeof(T);
};


template <Size currentMempos, class Arg, class ... Args>
struct _TotalSizeOfTypes
{
    static constexpr Size value = _TotalSizeOfTypes
            <
            TupleTypePlacementInfo<currentMempos, Arg>::newMempos,
            Args...
            >::value;
};

template <Size currentMempos, class Arg>
struct _TotalSizeOfTypes<currentMempos, Arg>
{
    static constexpr Size value = TupleTypePlacementInfo<currentMempos, Arg>::newMempos;
};


template <class ... Args>
struct TotalSizeOfTypes
{
    static constexpr Size value = _TotalSizeOfTypes<0, Args...>::value;
};

template <>
struct TotalSizeOfTypes<>
{
    static constexpr Size value = 0;
};


template <Size i, class Arg, class ... Args>
struct GetTypeShift
{
    static constexpr Size value = SizeOfType<Arg>::value + GetTypeShift<i - 1, Args...>::value;
};

template <class Arg, class ... Args>
struct GetTypeShift<0, Arg, Args...>
{
    static constexpr Size value = 0;
};


template <class ... Args>
struct Tuple
{
    using TypeOf = TypeList<Args...>;

private:
    template <class CArg, class ... CArgs>
    void Construct(const Size shift, CArg &&arg, CArgs &&...args)
    {
        new(static_cast<void*>(&_buffer[0] + shift)) CArg(forward<CArg>(arg));
        Construct<CArgs...>(shift + SizeOfType<CArg>::value, forward<CArgs>(args)...);
    };

    template <class CArg>
    void Construct(const Size shift, CArg &&arg)
    {
        new(static_cast<void*>(&_buffer[0] + shift)) CArg(forward<CArg>(arg));
    };

public:
    explicit Tuple(Args &&...args): _buffer{0}
    {
        Construct(0, forward<Args>(args)...);
    }

    template <Size i>
    auto at() const
    {
        using ResultType = typename TypeOf::template At<i>::Type;
        return *static_cast<const ResultType*>(static_cast<const void*>(&_buffer[0] + GetTypeShift<i, Args...>::value));
    }

private:
    char _buffer[TotalSizeOfTypes<Args...>::value];
};

template <>
struct Tuple<>
{
};


/*template <class Arg, class ... Args>
struct Tuple
{
    using TypeOf = TypeList<Arg, Args...>;

    constexpr explicit Tuple(Arg &&arg, Args &&...args):
            head(forward<Arg>(arg)),
            rest(forward<Args>(args)...)
    {
    }

    Arg head;
    Tuple<Args...> rest;
};

template <class Arg>
struct Tuple<Arg>
{
    using TypeOf = TypeList<Arg>;

    constexpr explicit Tuple(Arg &&arg):
            head(forward<Arg>(arg))
    {
    }

    Arg head;
    None rest;
};*/

template <class ... Args>
constexpr auto tuple(Args &&...args)
{
    return Tuple<Args...>{forward<Args>(args)...};
};


/*template <class Head, class Rest>
struct Tuple
{
    constexpr Tuple(Head &&head, Rest &&rest):
            head(forward<Head>(head)), rest(forward<Rest>(rest))
    {
    }

    Head head;
    Rest rest;
};*/

/*template <class Head, class Rest>
constexpr auto makeTupleNode(Head head, Rest rest)
{
    return Tuple<Head, Rest>{forward<Head>(head), forward<Rest>(rest)};
}

template <class Arg>
constexpr auto tuple(Arg arg)
{
    return makeTupleNode(forward<Arg>(arg), None{});
}

template <class Arg, class ... Args>
constexpr auto tuple(Arg arg, Args ...args)
{
    return makeTupleNode(forward<Arg>(arg), tuple(forward<Args>(args)...));
}

constexpr auto tuple()
{
    return None{};
}*/

/*template <class Functor>
void for_each(const empty_list_node &next, Functor func)
{
}

template <class List, class Functor>
void for_each(const List &list, Functor func)
{
    func(list.value);
    for_each(list.next, func);
}*/

}
