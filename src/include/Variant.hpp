#pragma once

#include "Move.hpp"
#include "Forward.hpp"
#include "type_traits/IsSame.hpp"
#include "type_traits/RemoveCV.hpp"
#include "TypeList.hpp"
#include <exception>


namespace stle
{

template <class ... Args>
class Variant;

template <template <class T> class Template>
struct RecursiveVariant
{
};

class BadVariantAccess : public std::exception
{
public:
    BadVariantAccess() = default;
    const char* what() const noexcept override
    {
        return "Bad variant access";
    }
};


/// Visitation

template <class T>
struct AbstractVariantVisitor
{
    virtual void visit(const T &value) = 0;
    virtual ~AbstractVariantVisitor() = default;
};

template <class ...Args>
struct AbstractCompositeVisitor : public AbstractVariantVisitor<Args>...
{
    using AbstractVariantVisitor<Args>::visit...;
};

template <class Result, class Function>
struct VisitorApplicatorBase
{
    VisitorApplicatorBase(Function &&function):
            function(std::forward<Function>(function))
    {
    }

    virtual ~VisitorApplicatorBase() = default;

    Result result;
    Function function;
};

template <class Result, class Function, class ...Args>
struct VisitorApplicator : VisitorApplicatorBase<Result, Function>
{
    using VisitorApplicatorBase<Result, Function>::VisitorApplicatorBase;
};

/// Setting value

template <class Base, class T>
struct ValueContainerAbstractSetter
{
    virtual Base* set(const T &value) = 0;
    virtual Base* set(T &&value) = 0;
};

template <class _Base, class ...Args>
struct CompositeValueContainerAbstractSetter : public ValueContainerAbstractSetter<_Base, Args>...
{
    using Base = _Base;

    using ValueContainerAbstractSetter<_Base, Args>::set...;
};

template <class Proto, class T, template <class Y> class Template>
struct ValueContainerSetterImplementor : virtual public Proto
{
    typename Proto::Base* set(const T &value) override
    {
        return new Template<T>{value};
    }

    typename Proto::Base* set(T &&value) override
    {
        return new Template<T>{std::move(value)};
    }
};


namespace details
{

template <class T>
struct RemoveCVRef
{
    using Type = typename RemoveCV<typename RemoveReference<T>::Type>::Type;
};

template<class T, class V>
struct VariantGetType
{
    using Type = T;
};

template<template<class T> class Template, class V>
struct VariantGetType<RecursiveVariant<Template>, V>
{
    using Type = Template<V>;
};

}


template <class ... Args>
class Variant
{
private:
    class ValueContainerBase;
    using Visitor = AbstractCompositeVisitor<typename details::VariantGetType<Args, Variant>::Type...>;
    using ValueContainerSetter = CompositeValueContainerAbstractSetter<ValueContainerBase, typename details::VariantGetType<Args, Variant>::Type...>;

    class ValueContainerBase : public virtual ValueContainerSetter
    {
    public:
        virtual void visit(Visitor &visitor) const = 0;
        virtual ValueContainerBase *copy() const = 0;
        virtual ~ValueContainerBase() = default;
    };

    template <class T>
    struct ValueContainer;

    template <class T>
    struct ValueContainer : public ValueContainerBase, public ValueContainerSetterImplementor<
            ValueContainerSetter,
            typename details::VariantGetType<Args, Variant>::Type,
            ValueContainer>...
    {
        using Base = ValueContainerBase;
        using StoredType = T;

        template <class Y>
        explicit ValueContainer(Y &&value): _value(stle::forward<Y>(value))
        {
        }

        void visit(Visitor &visitor) const override
        {
            visitor.visit(_value);
        }

        ValueContainerBase *copy() const override
        {
            return new ValueContainer{_value};
        }

        ValueContainerBase *set(const T &val) override
        {
            _value = val;
            return this;
        }

        ValueContainerBase *set(T &&val) override
        {
            _value = std::move(val);
            return this;
        }

        ~ValueContainer() override = default;

        T _value;
    };

public:
    using Types = TypeList<Args...>;
    using DefaultType = typename TypeList<typename details::VariantGetType<Args, Variant>::Type...>::Head;

    template <class T>
    using Has = typename Types::template Has<T>;

    Variant()
    {
        storeDefaultValue();
    }

    Variant(Variant &&another) noexcept
    {
        valueContainer = another.valueContainer;
        another.storeDefaultValue();
    }

    Variant(const Variant &another)
    {
        valueContainer = another.valueContainer->copy();
    }

    Variant(Variant &another) // self-explosion otherwise
    {
        valueContainer = another.valueContainer->copy();
    }

    template <class T>
    explicit Variant(T &&value)
    {
        valueContainer = new ValueContainer<typename details::RemoveCVRef<T>::Type>{stle::forward<T>(value)};
    };

    Variant &operator=(Variant &&another) noexcept
    {
        if (this != &another)
        {
            delete valueContainer;
            valueContainer = another.valueContainer;
            another.storeDefaultValue();
        }
        return *this;
    }

    Variant &operator=(const Variant &another)
    {
        if (this != &another)
        {
            delete valueContainer;
            valueContainer = another.valueContainer->copy();
        }
        return *this;
    }

    Variant &operator=(Variant &another) // self-explosion otherwise
    {
        if (this != &another)
        {
            delete valueContainer;
            valueContainer = another.valueContainer->copy();
        }
        return *this;
    }

    template <class T>
    Variant &operator=(T &&value)
    {
        auto ptr = valueContainer->set(std::forward<T>(value));
        if (ptr != valueContainer)
        {
            delete valueContainer;
        }
        valueContainer = ptr;
        return *this;
    };

    bool operator==(const Variant &another) = delete;
    bool operator!=(const Variant &another) = delete;

    ~Variant()
    {
        free();
    }

    template <class T>
    auto &as()
    {
        return getActualValueContainer<T>()->_value;
    }

    template <class T>
    const auto &as() const
    {
        return getActualValueContainer<T>()->_value;
    }

    template <class T>
    auto &get()
    {
        if (is<T>())
        {
            return getActualValueContainer<T>()->_value;
        }
        else
        {
            throw BadVariantAccess{};
        }
    }

    template <class T>
    const auto &get() const
    {
        if (is<T>())
        {
            return getActualValueContainer<T>()->_value;
        }
        else
        {
            throw BadVariantAccess{};
        }
    }

    void visit(Visitor &Visitor) const
    {
        valueContainer->visit(Visitor);
    }

    template <class Result, class Function>
    Result apply(Function &&function) const
    {
        using Applicator = VisitorApplicator<Result, Function, typename details::VariantGetType<Args, Variant>::Type...>;
        Applicator visitor{std::forward<Function>(function)};
        valueContainer->visit(visitor);
        return std::move(visitor.result);
    }

    template <class T>
    bool is() const noexcept
    {
        return apply<bool>([](const auto &v){
            using _Y = typename RemoveReference<decltype(v)>::Type;
            using Y = typename RemoveCV<_Y>::Type;
            return isSame<T, Y>;
        });
    }

    template <class Result, class First, class ...Rest>
    auto match(First first, Rest ...rest);

private:
    void storeDefaultValue()
    {
        valueContainer = new ValueContainer<DefaultType>(DefaultType{});
    }

    void free()
    {
        delete valueContainer;
        valueContainer = nullptr;
    }

    template <class T>
    auto getActualValueContainer() const
    {
        return static_cast<ValueContainer<T>*>(valueContainer);
    }

private:
    ValueContainerBase *valueContainer;
};


template <class ...Args>
bool isSameType(const Variant<Args...> &a, const Variant<Args...> &b)
{
    return a.template apply<bool>([&b](const auto &v){
        using _Y = typename RemoveReference<decltype(v)>::Type;
        using Y = typename RemoveCV<_Y>::Type;
        return b.template is<Y>();
    });
}

// === Pattern matching ============================================================================================= /

namespace match
{

template<template<class T> class Cond, class Func>
struct Expression
{
    template<class T>
    using Condition = Cond<T>;

    explicit Expression(Func &&func) : function(std::forward<Func>(func))
    {
    }

    Func function;
};

template<template<class T> class Cond>
struct ExpressionConstructor
{
    template<class Func>
    auto operator|(Func &&func) const
    {
        return Expression<Cond, Func>{stle::forward<Func>(func)};
    }

    template<class Value>
    auto operator|=(Value &&value) const
    {
        auto func = [value](const auto &v) { return value; };
        using Func = decltype(func);
        return Expression<Cond, Func>{move(func)};
    }
};

template<template<class T> class Cond>
const auto match = ExpressionConstructor<Cond>{};

template<class T>
struct _IsSameConditionExpr
{
    template<class _T>
    struct Template
    {
        static constexpr bool value = isSame<T, _T>;
    };
};

template<class T>
const auto match<_IsSameConditionExpr<T>::template Template> = ExpressionConstructor<_IsSameConditionExpr<T>::template Template>{};

template<class T>
const auto is = ExpressionConstructor<_IsSameConditionExpr<T>::template Template>{};


template<class _T>
struct _MatchAllConditionExpr
{
    static constexpr bool value = true;
};

const auto any = ExpressionConstructor<_MatchAllConditionExpr>{};

// presets
//const auto integral = ExpressionConstructor<stle::is_integral>{};
//const auto floatingPoint = ExpressionConstructor<stle::is_floating_point>{};
//const auto arithmetic = ExpressionConstructor<stle::is_arithmetic>{};

}

// Apply match impl

namespace details
{

template <bool cond>
struct _ApplyMatchFunctionCall
{
    template <class T, class Arg, class ...Args>
    static auto call(const T &val, Arg &arg, Args& ...args)
    {
        using TestType = typename TypeList<Args...>::Head;
        return _ApplyMatchFunctionCall<TestType::template Condition<T>::value>::call(val, args...);
    }

    template <class T, class Arg>
    static auto call(const T &val, Arg &arg)
    {
        return None{};
    }
};

template <>
struct _ApplyMatchFunctionCall<true>
{
    template <class T, class Arg, class ...Args>
    static auto call(const T &val, Arg &arg, Args& ...args)
    {
        return arg.function(val);
    }

    template <class T, class Arg>
    static auto call(const T &val, Arg &arg)
    {
        return arg.function(val);
    }
};

}

template<class ...Args>
template <class Result, class First, class ...Rest>
auto Variant<Args...>::match(First first, Rest ...rest)
{
    return this->template apply<Result>([&first, &rest...](const auto &val){
        using _T = typename std::remove_reference<decltype(val)>::type;
        using T = typename std::remove_cv<_T>::type;
        return details::_ApplyMatchFunctionCall<First::template Condition<T>::value>::call(val, first, rest...);
    });
}


// Template boiler-plate code section

// === VisitorApplicator visit func override presets =================================================================

template <class Result, class Function, class T0, class T1>
struct VisitorApplicator<Result, Function, T0, T1> :
        AbstractCompositeVisitor<T0, T1>,
        VisitorApplicatorBase<Result, Function>
{
    using  VisitorApplicatorBase<Result, Function>::VisitorApplicatorBase;

    void visit(const T0 &arg) override { this->result = this->function(arg); }
    void visit(const T1 &arg) override { this->result = this->function(arg); }
};

template <class Result, class Function, class T0, class T1, class T2>
struct VisitorApplicator<Result, Function, T0, T1, T2> :
        AbstractCompositeVisitor<T0, T1, T2>,
        VisitorApplicatorBase<Result, Function>
{
    using  VisitorApplicatorBase<Result, Function>::VisitorApplicatorBase;

    void visit(const T0 &arg) override { this->result = this->function(arg); }
    void visit(const T1 &arg) override { this->result = this->function(arg); }
    void visit(const T2 &arg) override { this->result = this->function(arg); }
};

template <class Result, class Function, class T0, class T1, class T2, class T3>
struct VisitorApplicator<Result, Function, T0, T1, T2, T3> :
        AbstractCompositeVisitor<T0, T1, T2, T3>,
        VisitorApplicatorBase<Result, Function>
{
    using  VisitorApplicatorBase<Result, Function>::VisitorApplicatorBase;

    void visit(const T0 &arg) override { this->result = this->function(arg); }
    void visit(const T1 &arg) override { this->result = this->function(arg); }
    void visit(const T2 &arg) override { this->result = this->function(arg); }
    void visit(const T3 &arg) override { this->result = this->function(arg); }
};

template <class Result, class Function, class T0, class T1, class T2, class T3, class T4>
struct VisitorApplicator<Result, Function, T0, T1, T2, T3, T4> :
        AbstractCompositeVisitor<T0, T1, T2, T3, T4>,
        VisitorApplicatorBase<Result, Function>
{
    using  VisitorApplicatorBase<Result, Function>::VisitorApplicatorBase;

    void visit(const T0 &arg) override { this->result = this->function(arg); }
    void visit(const T1 &arg) override { this->result = this->function(arg); }
    void visit(const T2 &arg) override { this->result = this->function(arg); }
    void visit(const T3 &arg) override { this->result = this->function(arg); }
    void visit(const T4 &arg) override { this->result = this->function(arg); }
};

template <class Result, class Function, class T0, class T1, class T2, class T3, class T4, class T5>
struct VisitorApplicator<Result, Function, T0, T1, T2, T3, T4, T5> :
        AbstractCompositeVisitor<T0, T1, T2, T3, T4, T5>,
        VisitorApplicatorBase<Result, Function>
{
    using  VisitorApplicatorBase<Result, Function>::VisitorApplicatorBase;

    void visit(const T0 &arg) override { this->result = this->function(arg); }
    void visit(const T1 &arg) override { this->result = this->function(arg); }
    void visit(const T2 &arg) override { this->result = this->function(arg); }
    void visit(const T3 &arg) override { this->result = this->function(arg); }
    void visit(const T4 &arg) override { this->result = this->function(arg); }
    void visit(const T5 &arg) override { this->result = this->function(arg); }
};

template <class Result, class Function, class T0, class T1, class T2, class T3, class T4, class T5, class T6>
struct VisitorApplicator<Result, Function, T0, T1, T2, T3, T4, T5, T6> :
        AbstractCompositeVisitor<T0, T1, T2, T3, T4, T5, T6>,
        VisitorApplicatorBase<Result, Function>
{
    using  VisitorApplicatorBase<Result, Function>::VisitorApplicatorBase;

    void visit(const T0 &arg) override { this->result = this->function(arg); }
    void visit(const T1 &arg) override { this->result = this->function(arg); }
    void visit(const T2 &arg) override { this->result = this->function(arg); }
    void visit(const T3 &arg) override { this->result = this->function(arg); }
    void visit(const T4 &arg) override { this->result = this->function(arg); }
    void visit(const T5 &arg) override { this->result = this->function(arg); }
    void visit(const T6 &arg) override { this->result = this->function(arg); }
};

template <class Result, class Function, class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7>
struct VisitorApplicator<Result, Function, T0, T1, T2, T3, T4, T5, T6, T7> :
        AbstractCompositeVisitor<T0, T1, T2, T3, T4, T5, T6, T7>,
        VisitorApplicatorBase<Result, Function>
{
    using  VisitorApplicatorBase<Result, Function>::VisitorApplicatorBase;

    void visit(const T0 &arg) override { this->result = this->function(arg); }
    void visit(const T1 &arg) override { this->result = this->function(arg); }
    void visit(const T2 &arg) override { this->result = this->function(arg); }
    void visit(const T3 &arg) override { this->result = this->function(arg); }
    void visit(const T4 &arg) override { this->result = this->function(arg); }
    void visit(const T5 &arg) override { this->result = this->function(arg); }
    void visit(const T6 &arg) override { this->result = this->function(arg); }
    void visit(const T7 &arg) override { this->result = this->function(arg); }
};

template <class Result, class Function, class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8>
struct VisitorApplicator<Result, Function, T0, T1, T2, T3, T4, T5, T6, T7, T8> :
        AbstractCompositeVisitor<T0, T1, T2, T3, T4, T5, T6, T7, T8>,
        VisitorApplicatorBase<Result, Function>
{
    using  VisitorApplicatorBase<Result, Function>::VisitorApplicatorBase;

    void visit(const T0 &arg) override { this->result = this->function(arg); }
    void visit(const T1 &arg) override { this->result = this->function(arg); }
    void visit(const T2 &arg) override { this->result = this->function(arg); }
    void visit(const T3 &arg) override { this->result = this->function(arg); }
    void visit(const T4 &arg) override { this->result = this->function(arg); }
    void visit(const T5 &arg) override { this->result = this->function(arg); }
    void visit(const T6 &arg) override { this->result = this->function(arg); }
    void visit(const T7 &arg) override { this->result = this->function(arg); }
    void visit(const T8 &arg) override { this->result = this->function(arg); }
};

template <class Result, class Function, class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9>
struct VisitorApplicator<Result, Function, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9> :
        AbstractCompositeVisitor<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>,
        VisitorApplicatorBase<Result, Function>
{
    using  VisitorApplicatorBase<Result, Function>::VisitorApplicatorBase;

    void visit(const T0 &arg) override { this->result = this->function(arg); }
    void visit(const T1 &arg) override { this->result = this->function(arg); }
    void visit(const T2 &arg) override { this->result = this->function(arg); }
    void visit(const T3 &arg) override { this->result = this->function(arg); }
    void visit(const T4 &arg) override { this->result = this->function(arg); }
    void visit(const T5 &arg) override { this->result = this->function(arg); }
    void visit(const T6 &arg) override { this->result = this->function(arg); }
    void visit(const T7 &arg) override { this->result = this->function(arg); }
    void visit(const T8 &arg) override { this->result = this->function(arg); }
    void visit(const T9 &arg) override { this->result = this->function(arg); }
};

template <class Result, class Function, class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9, class T10>
struct VisitorApplicator<Result, Function, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> :
        AbstractCompositeVisitor<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>,
        VisitorApplicatorBase<Result, Function>
{
    using  VisitorApplicatorBase<Result, Function>::VisitorApplicatorBase;

    void visit(const T0 &arg) override { this->result = this->function(arg); }
    void visit(const T1 &arg) override { this->result = this->function(arg); }
    void visit(const T2 &arg) override { this->result = this->function(arg); }
    void visit(const T3 &arg) override { this->result = this->function(arg); }
    void visit(const T4 &arg) override { this->result = this->function(arg); }
    void visit(const T5 &arg) override { this->result = this->function(arg); }
    void visit(const T6 &arg) override { this->result = this->function(arg); }
    void visit(const T7 &arg) override { this->result = this->function(arg); }
    void visit(const T8 &arg) override { this->result = this->function(arg); }
    void visit(const T9 &arg) override { this->result = this->function(arg); }
    void visit(const T10 &arg) override { this->result = this->function(arg); }
};

template <class Result, class Function, class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9, class T10, class T11>
struct VisitorApplicator<Result, Function, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> :
        AbstractCompositeVisitor<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>,
        VisitorApplicatorBase<Result, Function>
{
    using  VisitorApplicatorBase<Result, Function>::VisitorApplicatorBase;

    void visit(const T0 &arg) override { this->result = this->function(arg); }
    void visit(const T1 &arg) override { this->result = this->function(arg); }
    void visit(const T2 &arg) override { this->result = this->function(arg); }
    void visit(const T3 &arg) override { this->result = this->function(arg); }
    void visit(const T4 &arg) override { this->result = this->function(arg); }
    void visit(const T5 &arg) override { this->result = this->function(arg); }
    void visit(const T6 &arg) override { this->result = this->function(arg); }
    void visit(const T7 &arg) override { this->result = this->function(arg); }
    void visit(const T8 &arg) override { this->result = this->function(arg); }
    void visit(const T9 &arg) override { this->result = this->function(arg); }
    void visit(const T10 &arg) override { this->result = this->function(arg); }
    void visit(const T11 &arg) override { this->result = this->function(arg); }
};

template <class Result, class Function, class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9, class T10, class T11, class T12>
struct VisitorApplicator<Result, Function, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> :
        AbstractCompositeVisitor<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>,
        VisitorApplicatorBase<Result, Function>
{
    using  VisitorApplicatorBase<Result, Function>::VisitorApplicatorBase;

    void visit(const T0 &arg) override { this->result = this->function(arg); }
    void visit(const T1 &arg) override { this->result = this->function(arg); }
    void visit(const T2 &arg) override { this->result = this->function(arg); }
    void visit(const T3 &arg) override { this->result = this->function(arg); }
    void visit(const T4 &arg) override { this->result = this->function(arg); }
    void visit(const T5 &arg) override { this->result = this->function(arg); }
    void visit(const T6 &arg) override { this->result = this->function(arg); }
    void visit(const T7 &arg) override { this->result = this->function(arg); }
    void visit(const T8 &arg) override { this->result = this->function(arg); }
    void visit(const T9 &arg) override { this->result = this->function(arg); }
    void visit(const T10 &arg) override { this->result = this->function(arg); }
    void visit(const T11 &arg) override { this->result = this->function(arg); }
    void visit(const T12 &arg) override { this->result = this->function(arg); }
};

// === END

}
