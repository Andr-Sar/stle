#pragma once


namespace stle
{

namespace
{

template <unsigned int n>
struct SizeTypeHelper
{
    using Type = unsigned int;
};

template <>
struct SizeTypeHelper<8>
{
    using Type = unsigned long long;
};

}

using Size = typename SizeTypeHelper<sizeof(void*)>::Type;


struct InplaceType
{
    explicit InplaceType() = default;
};

inline constexpr InplaceType inplace{};

}
