#pragma once


namespace stle
{
namespace concepts
{

struct LiteralType
{
    static constexpr bool traitLiteralType = true;
};

}
}
