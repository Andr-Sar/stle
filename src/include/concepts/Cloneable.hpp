#pragma once


namespace stle
{
namespace concepts
{

template <class Self>
class Cloneable
{
public:
    virtual Self *clone() const = 0;
    ~Cloneable() = default;
};

}
}
