#pragma once

#include "memory/AllocatorInterface.hpp"


namespace stle
{

class Allocator
{
public:
    Allocator();

    explicit Allocator(memory::AllocatorInterface *allocator) noexcept: defaultAllocator(false)
    {
        interface = allocator;
    }

    template <class T>
    T *allocate()
    {
        return static_cast<T*>(interface->allocate(sizeof(T)));
    }

    template <class T>
    T *allocate(Size count)
    {
        return static_cast<T*>(interface->allocate(sizeof(T), count));
    }

    template <class T>
    T *reallocate(T *ptr, Size oldCount, Size newCount)
    {
        return static_cast<T*>(interface->reallocate(ptr, sizeof(T), oldCount, newCount));
    }

    template <class T>
    void free(T *ptr)
    {
        interface->free(ptr, sizeof(T));
    }

    template <class T>
    void free(T *ptr, Size count)
    {
        interface->free(ptr, sizeof(T), count);
    }

    memory::AllocatorStatistics getStatistics()
    {
        return interface->getStatistics();
    }

    void reset_statistics()
    {
        interface->resetStatistics();
    }

    void *allocateString(Size size)
    {
        return interface->allocateString(size);
    }

    void deallocateString(void *ptr, Size size)
    {
        return interface->deallocateString(ptr, size);
    }

    ~Allocator();

private:
    bool defaultAllocator;
    memory::AllocatorInterface *interface;
};

extern Allocator allocator;

}
