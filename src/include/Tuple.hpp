#pragma once

#include "Move.hpp"
#include "Forward.hpp"
#include "None.hpp"
#include "TypeList.hpp"
#include "BasicTypes.hpp"
#include "IndexSequence.hpp"


namespace stle
{

template <class ... Args>
struct Tuple
{
    using TypeOf = TypeList<Args...>;
};

template <class ... Args>
constexpr Tuple<Args...> tuple(Args &&...args);

///

template <class T, Size i, Size current>
struct GetImpl
{
    static constexpr auto &getConst(const T &t)
    {
        return GetImpl<decltype(t.rest), i, current + 1>::getConst(t.rest);
    }

    static auto &get(T &t)
    {
        return GetImpl<decltype(t.rest), i, current + 1>::get(t.rest);
    }
};

template <class T, Size i>
struct GetImpl<T, i, i>
{
    static constexpr auto &getConst(const T &t)
    {
        return t.head;
    }

    static auto &get(T &t)
    {
        return t.head;
    }
};

///

template <class Arg, class ... Args>
struct Tuple<Arg, Args...>
{
    using TypeOf = TypeList<Arg, Args...>;

    constexpr Tuple(Arg &&arg, Args &&...args):
            head(forward<Arg>(arg)),
            rest(forward<Args>(args)...)
    {
    }

    constexpr Tuple(const Tuple &another):
            head(another.head),
            rest(another.rest)
    {
    }

    Tuple(Tuple &&another) noexcept:
            head(forward<Arg>(another.head)),
            rest(move(another.rest))
    {
    }

    template <Size i>
    constexpr decltype(auto) at() const noexcept
    {
        return GetImpl<Tuple, i, 0>::getConst(*this);
    }

    template <Size i>
    decltype(auto) at() noexcept
    {
        return GetImpl<Tuple, i, 0>::get(*this);
    }

    constexpr auto &first() const noexcept
    {
        return head;
    }

    auto &first() noexcept
    {
        return head;
    }

    constexpr decltype(auto) last() const noexcept
    {
        return at<TypeOf::size - 1>();
    }

    decltype(auto) last() noexcept
    {
        return at<TypeOf::size - 1>();
    }

    template <class Func>
    constexpr auto expandTo(Func &&func) const
    {
        return expandToImpl(forward<Func>(func), typename MakeIndexSequence<sizeof...(Args) + 1>::Type{});
    }

    template <class Func>
    auto expandTo(Func &&func)
    {
        return expandToImpl(forward<Func>(func), typename MakeIndexSequence<sizeof...(Args) + 1>::Type{});
    }

    constexpr Size size() const noexcept
    {
        return sizeof...(Args) + 1;
    }

    Arg head;
    Tuple<Args...> rest;

private:
    template <class Func, Size ...indices>
    constexpr auto expandToImpl(Func &&func, IndexSequence<indices...>) const
    {
        return forward<Func>(func)(at<indices>()...);
    }

    template <class Func, Size ...indices>
    auto expandToImpl(Func &&func, IndexSequence<indices...>)
    {
        return forward<Func>(func)(at<indices>()...);
    }
};

template <class Arg>
struct Tuple<Arg>
{
    using TypeOf = TypeList<Arg>;

    constexpr explicit Tuple(Arg &&arg):
            head(forward<Arg>(arg))
    {
    }

    template <Size i>
    constexpr auto &at() const noexcept
    {
        return head;
    }

    template <Size i>
    auto &at() noexcept
    {
        return head;
    }

    constexpr Size size() const noexcept
    {
        return 1;
    }

    constexpr auto &first() const noexcept
    {
        return head;
    }

    auto &first() noexcept
    {
        return head;
    }

    constexpr auto &last() const noexcept
    {
        return head;
    }

    auto &last() noexcept
    {
        return head;
    }

    template <class Func>
    constexpr auto expandTo(Func &&func) const
    {
        return forward<Func>(func)(head);
    }

    Arg head;
    None rest;
};

template <>
struct Tuple<>
{
    None head;
    None rest;

    using TypeOf = TypeList<>;

    constexpr Size size() const noexcept
    {
        return 0;
    }
};


template <class ... Args>
constexpr Tuple<Args...> tuple(Args &&...args)
{
    return Tuple<Args...>{forward<Args>(args)...};
};

template <>
constexpr Tuple<> tuple<>()
{
    return Tuple<>{};
}


template <class ...Args1, class ...Args2, Size ...indices1, Size ...indices2>
constexpr Tuple<Args1..., Args2...> concatImpl(const Tuple<Args1...> &a,
                                               const Tuple<Args2...> &b,
                                               IndexSequence<indices1...> ind1,
                                               IndexSequence<indices2...> ind2)
{
    return {Args1{a.template at<indices1>()}..., Args2{b.template at<indices2>()}...};
}

template <class ...Args1, class ...Args2>
constexpr Tuple<Args1..., Args2...> concat(const Tuple<Args1...> &a, const Tuple<Args2...> &b)
{
    return concatImpl(a, b,
                      typename MakeIndexSequence<sizeof...(Args1)>::Type{},
                      typename MakeIndexSequence<sizeof...(Args2)>::Type{}
    );
}

///

template <class T1, class T2, Size ...indices1, Size ...indices2>
auto concatUniversalImpl(T1 &&a, T2 &&b,
                         IndexSequence<indices1...> ind1,
                         IndexSequence<indices2...> ind2)
{
    return tuple(a.template at<indices1>()..., b.template at<indices2>()...);
}

template <class T1, class T2>
auto concat(T1 &&a, T2 &&b)
{
    using X = typename RemoveReference<T1>::Type::TypeOf;
    using Y = typename RemoveReference<T2>::Type::TypeOf;
    return concatUniversalImpl(forward<T1>(a), forward<T2>(b),
                      typename MakeIndexSequence<X::size>::Type{},
                      typename MakeIndexSequence<Y::size>::Type{}
    );
}

auto concat(Tuple<> a, Tuple<> b)
{
    return Tuple<>{};
};

///

template <class Func, class ... Args, Size ...indices>
auto tupleMapImpl(const Tuple<Args...> &t, Func &&func, IndexSequence<indices...>)
{
    return Tuple<Args...>{forward<Func>(func)(t.template at<indices>()...)};
}

template <class Func, class ... Args>
auto map(const Tuple<Args...> &t, Func &&func)
{
    return tupleMapImpl(t, forward<Func>(func), typename MakeIndexSequence<sizeof...(Args)>::Type{});
}

///

template <class TupleType>
struct TupleFoldImplementation
{
    template <class T, class Func>
    static T foldl(const TupleType &t, Func &&func, T accumulated)
    {
        return TupleFoldImplementation<decltype(t.rest)>::foldl(t.rest, forward<Func>(func), func(accumulated, t.head));
    }

    template <class T, class Func>
    static T fold(const TupleType &t, Func &&func, T accumulated)
    {
        return func(TupleFoldImplementation<decltype(t.rest)>::fold(t.rest, forward<Func>(func), accumulated), t.head);
    }
};

template <>
struct TupleFoldImplementation<None>
{
    template <class T, class Func>
    static T foldl(const None &t, Func &&func, T accumulated)
    {
        return accumulated;
    }

    template <class T, class Func>
    static T fold(const None &t, Func &&func, T accumulated)
    {
        return accumulated;
    }
};

template <class T, class Func, class ... Args>
T foldl(const Tuple<Args...> &t, Func &&func, T init = {})
{
    return TupleFoldImplementation<Tuple<Args...>>::template foldl(t, forward<Func>(func), init);
}

template <class T, class Func, class ... Args>
T fold(const Tuple<Args...> &t, Func &&func, T init = {})
{
    return TupleFoldImplementation<Tuple<Args...>>::template fold(t, forward<Func>(func), init);
}

}
