#pragma once

#include "../BasicTypes.hpp"
#include "Allocator.hpp"
#include "../memory/Copy.hpp"
#include "../memory/Move.hpp"
#include "../memory/Clear.hpp"


namespace stle
{

template <class T>
class DynamicVector
{
public:
    using ValueType = T;
    using SizeType = unsigned int;

    static constexpr SizeType startingCapacity = 8;

    DynamicVector() noexcept:
        size(0),
        capacity(0),
        data(nullptr)
    {

    }

    template <class ...Rest>
    explicit DynamicVector(const T& arg, const Rest&... args): DynamicVector()
    {
        allocateInitially(1 + sizeof...(args));
        constructorPushBack(arg, args...);
    }

    DynamicVector(const DynamicVector &another):
            size(another.size),
            capacity(another.size),
            data(nullptr)
    {
        if (!another.isEmpty())
        {
            data = allocator.allocate<T>(size);
            copyDataFromAnother(another);
        }
    }

    DynamicVector(DynamicVector &&another) noexcept :
            size(another.size),
            capacity(another.capacity),
            data(another.data)
    {
        another.nullify();
    }

    DynamicVector &operator=(const DynamicVector &another)
    {
        if (this != &another)
        {
            resetMemory();

            data = allocator.allocate<T>(another.size);
            size = another.size;
            capacity = another.size;
            copyDataFromAnother(another);
        }

        return *this;
    }

    DynamicVector &operator=(DynamicVector &&another) noexcept
    {
        resetMemory();

        size = another.size;
        capacity = another.capacity;
        data = another.data;

        another.nullify();

        return *this;
    }

    void clear()
    {
        clearMemory();
        size = 0;
    }

    ~DynamicVector()
    {
        resetMemory();
    }

    bool isEmpty() const noexcept
    {
        return size == 0;
    }

    bool isNotEmpty() const noexcept
    {
        return size > 0;
    }

    SizeType getSize() const noexcept
    {
        return size;
    }

    SizeType getCapacity() const noexcept
    {
        return capacity;
    }

    T *getData() noexcept
    {
        data;
    }

    const T *getData() const noexcept
    {
        return data;
    }

    T &operator[](SizeType index)
    {
        return data[index];
    }

    const T &operator[](SizeType index) const
    {
        return data[index];
    }

    T &getFront()
    {
        return *data;
    }

    const T &getFront() const
    {
        return *data;
    }

    T &getBack()
    {
        return data[size - 1];
    }

    const T &getBack() const
    {
        return data[size - 1];
    }

    void clearAndReset()
    {
        resetMemory();
        nullify();
    }

    void pushBack(const T &value)
    {
        ensureSufficientCapacity();
        new(data + size) T(value);
        ++size;
    }

    void pushBack(T &&value)
    {
        ensureSufficientCapacity();
        new(data + size) T(move(value));
        ++size;
    }

private:
    void copyDataFromAnother(const DynamicVector &another)
    {
        memory::copy(another.data, data, size);
    }

    void freeMemory()
    {
        allocator.free(data, capacity);
    }

    void clearMemory()
    {
        memory::clear(data, size);
    }

    void resetMemory()
    {
        if (isNotEmpty())
        {
            clearMemory();
            freeMemory();
        }
    }

    void nullify()
    {
        size = 0;
        capacity = 0;
        data = nullptr;
    }

    void ensureSufficientCapacity()
    {
        if (size == capacity)
        {
            if (size > 0)
            {
                const SizeType newCapacity = capacity * 2;
                T *newData = allocator.reallocate(data, capacity, newCapacity);
                capacity = newCapacity;
                data = newData;
            }
            else
            {
                allocateInitially(startingCapacity);
            }
        }
    }

    void allocateInitially(SizeType n)
    {
        capacity = n;
        data = allocator.allocate<T>(n);
    }

    template <class ...Rest>
    void constructorPushBack(const T& arg, const Rest&... args)
    {
        data[size] = arg;
        ++size;
        constructorPushBack(args...);
    }

    void constructorPushBack(const T& arg)
    {
        data[size] = arg;
        ++size;
    }

private:
    SizeType size;
    SizeType capacity;
    T *data;
};

}
