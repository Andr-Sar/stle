#pragma once

#include "../BasicTypes.hpp"
#include "Allocator.hpp"
#include "Forward.hpp"
#include "../memory/Copy.hpp"
#include "../memory/Move.hpp"
#include "../memory/Clear.hpp"


namespace stle
{

template <class T>
class Vector
{
public:
    using ValueType = T;
    using SizeType = Size;

public:
    Vector() noexcept:
            data(nullptr),
            size(0)
    {
    }

    template <class Arg, class ...Args>
    explicit Vector(Arg &&arg, Args &&...args):
            data(allocator.allocate<T>(1 + sizeof...(args))),
            size(0)
    {
        constructorPushBack(forward<Arg>(arg), forward<Args>(args)...);
    }

    Vector(const Vector &another):
            data(nullptr),
            size(another.size)
    {
        if (another.isNotEmpty())
        {
            data = allocator.allocate<T>(size);
            copyDataFromAnother(another);
        }
    }

    Vector(Vector &&another) noexcept:
            data(another.data),
            size(another.size)
    {
        another.nullify();
    }

    Vector &operator=(const Vector &another)
    {
        if (this != &another)
        {
            resetMemory();

            data = allocator.allocate<T>(another.size);
            size = another.size;
            copyDataFromAnother(another);
        }

        return *this;
    }

    Vector &operator=(Vector &&another) noexcept
    {
        resetMemory();

        size = another.size;
        data = another.data;

        another.nullify();

        return *this;
    }

    ~Vector()
    {
        resetMemory();
    }

    bool isEmpty() const noexcept
    {
        return size == 0;
    }

    bool isNotEmpty() const noexcept
    {
        return size > 0;
    }

    SizeType getSize() const noexcept
    {
        return size;
    }

    T &operator[](SizeType index)
    {
        return data[index];
    }

    const T &operator[](SizeType index) const
    {
        return data[index];
    }

    T &getFront()
    {
        return *data;
    }

    const T &getFront() const
    {
        return *data;
    }

    T &getBack()
    {
        return data[size - 1];
    }

    const T &getBack() const
    {
        return data[size - 1];
    }

    void clear()
    {
        resetMemory();
        nullify();
    }

private:
    void copyDataFromAnother(const Vector &another)
    {
        memory::copy(another.data, data, size);
    }

    void freeMemory()
    {
        allocator.free(data, size);
    }

    void clearMemory()
    {
        memory::clear(data, size);
    }

    void resetMemory()
    {
        if (isNotEmpty())
        {
            clearMemory();
            freeMemory();
        }
    }

    void nullify()
    {
        data = nullptr;
        size = 0;
    }

    template <class ...Rest>
    void constructorPushBack(T&& arg, Rest&&... args)
    {
        data[size] = forward<T>(arg);
        ++size;
        constructorPushBack(forward<Rest>(args)...);
    }

    void constructorPushBack(T&& arg)
    {
        data[size] = forward<T>(arg);
        ++size;
    }

private:
    T *data;
    Size size;
};

}
