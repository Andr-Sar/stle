#pragma once


namespace stle
{

template <class T>
constexpr bool isIntegral()
{
    return false;
}

template <>
inline constexpr bool isIntegral<int>() { return true; }

template <>
inline constexpr bool isIntegral<unsigned int>() { return true; }

template <>
inline constexpr bool isIntegral<short>() { return true; }

template <>
inline constexpr bool isIntegral<unsigned short>() { return true; }

template <>
inline constexpr bool isIntegral<long>() { return true; }

template <>
inline constexpr bool isIntegral<unsigned long>() { return true; }

template <>
inline constexpr bool isIntegral<long long>() { return true; }

template <>
inline constexpr bool isIntegral<unsigned long long>() { return true; }

template <>
inline constexpr bool isIntegral<char>() { return true; }

template <>
inline constexpr bool isIntegral<signed char>() { return true; }

template <>
inline constexpr bool isIntegral<unsigned char>() { return true; }

template <>
inline constexpr bool isIntegral<bool>() { return true; }

template <>
inline constexpr bool isIntegral<wchar_t >() { return true; }

template <>
inline constexpr bool isIntegral<char16_t >() { return true; }

template <>
inline constexpr bool isIntegral<char32_t >() { return true; }

// const

template <>
inline constexpr bool isIntegral<const int>() { return true; }

template <>
inline constexpr bool isIntegral<const unsigned int>() { return true; }

template <>
inline constexpr bool isIntegral<const short>() { return true; }

template <>
inline constexpr bool isIntegral<const unsigned short>() { return true; }

template <>
inline constexpr bool isIntegral<const long>() { return true; }

template <>
inline constexpr bool isIntegral<const unsigned long>() { return true; }

template <>
inline constexpr bool isIntegral<const long long>() { return true; }

template <>
inline constexpr bool isIntegral<const unsigned long long>() { return true; }

template <>
inline constexpr bool isIntegral<const char>() { return true; }

template <>
inline constexpr bool isIntegral<const signed char>() { return true; }

template <>
inline constexpr bool isIntegral<const unsigned char>() { return true; }

template <>
inline constexpr bool isIntegral<const bool>() { return true; }

template <>
inline constexpr bool isIntegral<const wchar_t >() { return true; }

template <>
inline constexpr bool isIntegral<const char16_t >() { return true; }

template <>
inline constexpr bool isIntegral<const char32_t >() { return true; }

// volatile

template <>
inline constexpr bool isIntegral<volatile int>() { return true; }

template <>
inline constexpr bool isIntegral<volatile unsigned int>() { return true; }

template <>
inline constexpr bool isIntegral<volatile short>() { return true; }

template <>
inline constexpr bool isIntegral<volatile unsigned short>() { return true; }

template <>
inline constexpr bool isIntegral<volatile long>() { return true; }

template <>
inline constexpr bool isIntegral<volatile unsigned long>() { return true; }

template <>
inline constexpr bool isIntegral<volatile long long>() { return true; }

template <>
inline constexpr bool isIntegral<volatile unsigned long long>() { return true; }

template <>
inline constexpr bool isIntegral<volatile char>() { return true; }

template <>
inline constexpr bool isIntegral<volatile signed char>() { return true; }

template <>
inline constexpr bool isIntegral<volatile unsigned char>() { return true; }

template <>
inline constexpr bool isIntegral<volatile bool>() { return true; }

template <>
inline constexpr bool isIntegral<volatile wchar_t >() { return true; }

template <>
inline constexpr bool isIntegral<volatile char16_t >() { return true; }

template <>
inline constexpr bool isIntegral<volatile char32_t >() { return true; }

}
