#pragma once


namespace stle
{

template <class T>
struct RemoveConst
{
    using Type = T;
};

template <class T>
struct RemoveConst<const T>
{
    using Type = T;
};


template <class T>
struct RemoveVolatile
{
    using Type = T;
};

template <class T>
struct RemoveVolatile<volatile T>
{
    using Type = T;
};


template <class T>
struct RemoveCV
{
    using Type = typename RemoveVolatile<typename RemoveConst<T>::Type>::Type;
};


}
