#pragma once

#include "IsBaseOf.hpp"
#include "IsScalar.hpp"
#include "../concepts/LiteralType.hpp"


namespace stle
{

template <class T>
constexpr bool isLiteralType = isBaseOf<concepts::LiteralType, T> || isScalar<T>;

}
