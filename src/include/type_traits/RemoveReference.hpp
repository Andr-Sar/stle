#pragma once


namespace stle
{

template <class T>
struct RemoveReference
{
    using Type = T;
};

template <class T>
struct RemoveReference<T&>
{
    using Type = T;
};

template <class T>
struct RemoveReference<T&&>
{
    using Type = T;
};

}
