#pragma once


namespace stle
{

template <class T>
constexpr bool isVoid()
{
    return false;
}

template <>
inline constexpr bool isVoid<void>() { return true; }

template <>
inline constexpr bool isVoid<const void>() { return true; }

template <>
inline constexpr bool isVoid<volatile void>() { return true; }

template <>
inline constexpr bool isVoid<const volatile void>() { return true; }

}
