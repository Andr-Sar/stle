#pragma once


namespace stle
{

template <class T>
constexpr bool isPointer = false;

template <class T>
constexpr bool isPointer<T*> = true;

template <class T>
constexpr bool isPointer<T *const> = true;

template <class T>
constexpr bool isPointer<const T *> = true;

}
