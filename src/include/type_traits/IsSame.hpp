#pragma once


namespace stle
{

template <class T, class U>
constexpr bool isSame = false;

template <class T>
constexpr bool isSame<T, T> = true;

}
