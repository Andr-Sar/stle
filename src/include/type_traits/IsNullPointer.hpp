#pragma once

//#include "IsSame.hpp"


namespace stle
{

template <class T>
constexpr bool isNullPointer = false;

template <>
constexpr bool isNullPointer<decltype(nullptr)> = true;

}
