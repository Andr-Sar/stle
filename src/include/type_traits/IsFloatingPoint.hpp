#pragma once


namespace stle
{

template <class T>
constexpr bool isFloatingPoint()
{
    return false;
}

template <>
inline constexpr bool isFloatingPoint<float>() { return true; }

template <>
inline constexpr bool isFloatingPoint<double>() { return true; }

template <>
inline constexpr bool isFloatingPoint<long double>() { return true; }

// const

template <>
inline constexpr bool isFloatingPoint<const float>() { return true; }

template <>
inline constexpr bool isFloatingPoint<const double>() { return true; }

template <>
inline constexpr bool isFloatingPoint<const long double>() { return true; }

// volatile

template <>
inline constexpr bool isFloatingPoint<volatile float>() { return true; }

template <>
inline constexpr bool isFloatingPoint<volatile double>() { return true; }

template <>
inline constexpr bool isFloatingPoint<volatile long double>() { return true; }

}
