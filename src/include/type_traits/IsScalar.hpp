#pragma once

#include "IsArithmetic.hpp"
#include "IsPointer.hpp"
#include "IsNullPointer.hpp"


namespace stle
{

template <class T>
constexpr bool isScalar = isArithmetic<T> || isPointer<T> || isNullPointer<T>;

}
