#pragma once


namespace stle
{

namespace
{

template<class T>
constexpr bool _isBaseOf(const volatile T *p)
{
    return true;
}

template<class T>
constexpr bool _isBaseOf(const volatile void *p)
{
    return false;
}

}

/*template <class Base, class Derived>
constexpr bool isBaseOf()
{
    return _isBaseOf<Base>(static_cast<const Derived*>(nullptr));
}*/

template <class Base, class Derived>
constexpr bool isBaseOf = _isBaseOf<Base>(static_cast<const Derived*>(nullptr));

}
