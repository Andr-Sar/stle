#pragma once


namespace stle
{

template <class T>
constexpr bool isLvalueReference = false;

template <class T>
constexpr bool isLvalueReference<T&> = true;

template <class T>
constexpr bool isLvalueReference<const T&> = true;

template <class T>
constexpr bool isRvalueReference = false;

template <class T>
constexpr bool isRvalueReference<T&&> = true;

}
