#pragma once

#include "IsIntegral.hpp"
#include "IsFloatingPoint.hpp"


namespace stle
{

template <class T>
constexpr bool isArithmetic = isIntegral<T>() || isFloatingPoint<T>();

}
