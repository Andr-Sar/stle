#pragma once

#include "type_traits/RemoveReference.hpp"
#include "type_traits/IsReference.hpp"


namespace stle
{

template <class T>
constexpr T&& forward(typename RemoveReference<T>::Type& t) noexcept
{
    return static_cast<T&&>(t);
}

template <class T>
constexpr T&& forward(typename RemoveReference<T>::Type&& t) noexcept
{
    static_assert(!isLvalueReference<T>, "Can not forward an rvalue as an lvalue.");
    return static_cast<T&&>(t);
}

}
