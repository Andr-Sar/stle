#pragma once

#include <exception>


namespace stle
{

class Exception : public std::exception
{
public:
    ~Exception() override = default;
};

}
