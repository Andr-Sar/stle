#pragma once

#include "BasicTypes.hpp"
#include "Move.hpp"
#include "Forward.hpp"
#include "type_traits/IsLiteralType.hpp"
#include <exception>


namespace stle
{

template <class T>
class Optional;

struct NulloptType
{
    enum class _Construct { _Token };
    explicit constexpr NulloptType(_Construct) noexcept { }
};

inline constexpr NulloptType nullopt { NulloptType::_Construct::_Token };


class BadOptionalAccess : public std::exception
{
public:
    BadOptionalAccess() = default;
    const char* what() const noexcept override
    {
        return "Bad optional access";
    }
};

namespace
{
[[noreturn]]
inline void throwBadOptionalAccess()
{
    throw BadOptionalAccess();
}
}

namespace optional_details
{

struct Dummy
{
};

template <class V>
struct ConstructorTag
{
};


template <class T, bool _isLiteralType = isLiteralType<T> >
struct PayloadImplementation
{
    bool valid;
    union
    {
        Dummy dummy;
        T object;
    };

    constexpr PayloadImplementation() noexcept: valid(false), dummy()
    {
    }

    constexpr PayloadImplementation(ConstructorTag<bool>, const T &obj): valid(true), object{obj}
    {
    }

    constexpr PayloadImplementation(ConstructorTag<bool>, T &&obj) noexcept: valid(true), object{move(obj)}
    {
    }

    template <class ...Args>
    constexpr explicit PayloadImplementation(InplaceType, Args &&...args): valid(true), object(forward<Args>(args)...)
    {
    }

    constexpr explicit PayloadImplementation(ConstructorTag<void>) noexcept: valid(false), dummy()
    {
    }

    constexpr PayloadImplementation(const PayloadImplementation &another):
        PayloadImplementation(another.valid ? PayloadImplementation(ConstructorTag<bool>(), another.object) : PayloadImplementation(ConstructorTag<void>()))
    {
    }

    constexpr PayloadImplementation(PayloadImplementation &&another) noexcept:
        PayloadImplementation(another.valid ? PayloadImplementation(ConstructorTag<bool>(), move(another.object)) : PayloadImplementation(ConstructorTag<void>()))
    {
    }

    void reset() noexcept
    {
        valid = false;
    }
};

template <class T>
struct PayloadImplementation<T, false>
{
    bool valid;
    union
    {
        Dummy dummy;
        T object;
    };

    constexpr PayloadImplementation() noexcept: valid(false), dummy()
    {
    }

    constexpr PayloadImplementation(ConstructorTag<bool>, const T &obj): valid(true), object(obj)
    {
    }

    constexpr PayloadImplementation(ConstructorTag<bool>, T &&obj) noexcept: valid(true), object(move(obj))
    {
    }

    template <class ...Args>
    constexpr explicit PayloadImplementation(InplaceType, Args &&...args): valid(true), object(forward<Args>(args)...)
    {
    }

    constexpr explicit PayloadImplementation(ConstructorTag<void>) noexcept: valid(false), dummy()
    {
    }

    constexpr PayloadImplementation(const PayloadImplementation &another):
            PayloadImplementation(another.valid ? PayloadImplementation(ConstructorTag<bool>(), another.object) : PayloadImplementation(ConstructorTag<void>()))
    {
    }

    constexpr PayloadImplementation(PayloadImplementation &&another) noexcept:
            PayloadImplementation(another.valid ? PayloadImplementation(ConstructorTag<bool>(), move(another.object)) : PayloadImplementation(ConstructorTag<void>()))
    {
    }

    void reset() noexcept
    {
        if (valid)
        {
            object.~T();
            valid = false;
        }
    }

    ~PayloadImplementation()
    {
        if (valid)
        {
            object.~T();
        }
    }
};

}


template <class T>
class Optional
{
public:
    using StoredType = T;

public:
    constexpr Optional() noexcept = default;

    constexpr Optional(NulloptType) noexcept
    {
    }

    explicit constexpr Optional(const T &value):
        payload(optional_details::ConstructorTag<bool>(), value)
    {
    }

    explicit constexpr Optional(T &&value) noexcept:
        payload(optional_details::ConstructorTag<bool>(), move(value))
    {
    }

    constexpr Optional(const Optional &another):
        payload(another.payload)
    {
    }

    constexpr Optional(Optional &&another) noexcept:
        payload(move(another.payload))
    {
    }

    template <class ...Args>
    constexpr explicit Optional(InplaceType, Args &&...args):
        payload(inplace, forward<Args>(args)...)
    {
    }

    constexpr Optional& operator=(const Optional& another)
    {
        if (another)
        {
            if (this->hasValue())
                this->payload.object = another.payload.object;
            else
            {
                this->payload.object = another.payload.object;
                this->payload.valid = true;
            }
        }
        else
        {
            this->reset();
        }

        return *this;
    }

    constexpr Optional& operator=(Optional &&another) noexcept
    {
        if (another)
        {
            if (this->hasValue())
                this->payload.object = move(another.payload.object);
            else
            {
                this->payload.object = move(another.payload.object);
                this->payload.valid = true;
            }
        }
        else
        {
            this->reset();
        }

        return *this;
    }

    constexpr explicit operator bool() const noexcept
    {
        return payload.valid;
    }

    constexpr bool hasValue() const noexcept
    {
        return payload.valid;
    }

    constexpr T& value() &
    {
        return hasValue()
               ?  this->get()
               : (throwBadOptionalAccess(), this->get());
    }

    constexpr const T & value() const &
    {
        return hasValue()
               ?  this->get()
               : (throwBadOptionalAccess(), this->get());
    }

    constexpr T&& value() &&
    {
        return hasValue()
               ?  move(this->get())
               : (throwBadOptionalAccess(), move(this->get()));
    }

    constexpr const T&& value() const &&
    {
        return hasValue()
               ?  move(this->get())
               : (throwBadOptionalAccess(), move(this->get()));
    }

    template<typename U>
    constexpr T valueOr(U &&v) const&
    {
        return hasValue()
               ? this->get()
               : static_cast<T>(forward<U>(v));
    }

    template<typename U>
    T valueOr(U &&v) &&
    {
        return hasValue()
               ? move(this->get())
               : static_cast<U>(forward<U>(v));
    }

    constexpr const T& operator*() const&
    {
        return this->get();
    }

    constexpr T& operator*() &
    {
        return this->get();
    }

    constexpr T&& operator*()&&
    {
        return move(this->get());
    }

    constexpr const T&& operator*() const &&
    {
        return move(this->get());
    }

    void reset() noexcept
    {
        payload.reset();
    }

    ~Optional() = default;

protected:
    constexpr T& get() noexcept
    {
        return this->payload.object;
    }

    constexpr const T& get() const noexcept
    {
        return this->payload.object;
    }

private:
    optional_details::PayloadImplementation<T> payload;
};


template <class T>
constexpr auto makeOptional(T &&value)
{
    return Optional<T>{forward<T>(value)};
}

}
